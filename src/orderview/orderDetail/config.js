
//截取模块依赖的接口 需监控的css、js

//需要截取的模块（div），依赖的接口
//公共模块
var commonMod = [
    {
        withoutJudgePort:10*1000,//不等待接口，value值为等待时间
        //订单详情页依赖的url
        url: 'default?',
        //url对应的正则
        urlRe: /default\?/,
        //是否发起请求，暂时未用该数据
        isReqSucc: false,
        //是否接收到返回 默认false
        isResSucc: false

    },
];

//详情页对应接口  http://orderdetailsearch.tars.fws.qa.nt.ctripcorp.com/orderdetailsearch/api/json/metadata?op=OpenOrderDetailSearch
//各个模块对应所需判断的接口
var ajaxHash = {

    //退改说明模块
    'publicheader':[{
        // withoutJudgePort: 10*1000,//不等待接口，value值为等待时间,单位毫秒
        url:'default?',
        urlRe:/default\?/,
        isReqSucc:false,
        isResSucc:false
    },
    ]
};

//需要监控的css
var cssData = [
    // /detail(\.min)?\.css/,
    // /detail\-base(\.min)?\.css/
];
//需要监控的js
var jsData = [
    // /common\.js/,
    // /orderdetail\-flight\.js/,
    // /lizard\.lite\.min\.js/
    //XDetailCoupon\.js/,
];
//添加公共模块  将公共模块和个模块需要的数据合并
for (var item in ajaxHash) {
    ajaxHash[item] = ajaxHash[item].concat(commonMod);
}

module.exports = {
    ajaxHash: ajaxHash,
    cssData: cssData,
    jsData: jsData
};