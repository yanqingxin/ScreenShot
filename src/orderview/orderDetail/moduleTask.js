
//截图任务task 按模块来写对应的函数


var utils = require('../../common/utils');
var computeSize = utils.computeSize;
var loggerInfo = utils.loggerInfo;
var loggerTime = utils.loggerTime;
var loggerDebug = utils.loggerDebug;
var computeUseTime = utils.computeUseTime;
var consoleData = {};

module.exports = {
    'publicheader':function publicheader(page, dataInfo) {
        console.log('start publicheader() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息
        var pubLeaderSize = page.evaluate(function () {
            var select = [];
            var pubLeader = document.querySelectorAll('.public_header');
            var item, position;
            if (pubLeader && pubLeader.length) {
                //获取需要截取模块的位置信息
                item = pubLeader[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height, //|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    pubLeader[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (pubLeaderSize && pubLeaderSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, pubLeaderSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'pubLeader_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save pubLeader',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'pubLeader_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save pubLeader',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'pubLeader_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save pubLeader ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'orderinfobar':function orderinfobar(page, dataInfo) {
        console.log('start orderinfobar() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息
        var orderinfoSize = page.evaluate(function () {
            var select = [];
            var odinfo = document.querySelectorAll('.detail_top.clearfix');
            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height, //|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (orderinfoSize && orderinfoSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, orderinfoSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'orderinfo_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save pubLeader',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'orderinfo_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save orderinfo',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'orderinfo_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save orderinfo ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'infocustomer':function infocustomer(page, dataInfo) {
        console.log('start infocustomer() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息
        var infocustomerSize = page.evaluate(function () {
            var select = [];
            var odinfo = document.querySelectorAll('body > form:nth-child(7) > div.wrapper.theme_lightblue > div.detail_center.clearfix > div.detail_main.clearfix > div.detail_main_info > div > span:nth-child(3)');
            //var odinfo= document.querySelectorAll('body > form:nth-child(7) > div.wrapper.theme_lightblue > div.detail_center.clearfix > div.detail_main.clearfix > div.detail_top.clearfix');
            var item, position;
            if (odinfo && odinfo.length) {
            //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (infocustomerSize && infocustomerSize.length) {
        //返回有数据
        //设置phantomjs截图位置
            page.clipRect = computeSize(page, infocustomerSize, 'normal');
            if (page.clipRect.height != 0) {
            //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'infocustomer_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save infocustomer',consoleData);
            } else {
            //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'infocustomer_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save infocustomer',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'infocustomer_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save infocustomer ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'infocustomer2':function infocustomer2(page, dataInfo) {
        console.log('start infocustomer2() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息
        var infocustomer2Size = page.evaluate(function () {
            var select = [];
            var odinfo = document.querySelectorAll('body > form:nth-child(7) > div.wrapper.theme_lightblue > div.detail_center.clearfix > div.detail_main.clearfix > div.detail_main_info > div > span.right_bar.clearfix');
            //var odinfo= document.querySelectorAll('body > form:nth-child(7) > div.wrapper.theme_lightblue > div.detail_center.clearfix > div.detail_main.clearfix > div.detail_top.clearfix');
            var item, position;
            if (odinfo && odinfo.length) {
            //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (infocustomer2Size && infocustomer2Size.length) {
        //返回有数据
        //设置phantomjs截图位置
            page.clipRect = computeSize(page, infocustomer2Size, 'normal');
            if (page.clipRect.height != 0) {
            //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'infocustomer2_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save infocustomer2',consoleData);
            } else {
            //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'infocustomer2_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save infocustomer2',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'infocustomer2_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save infocustomer2 ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'maininfo':function maininfo(page, dataInfo) {
        console.log('start maininfo() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息
        var maininfoSize = page.evaluate(function () {
            var select = [];
            var odinfo = document.querySelectorAll('body > form:nth-child(7) > div.wrapper.theme_lightblue > div.detail_center.clearfix > div.detail_main.clearfix > div.detail_main_info > table');
            //var odinfo= document.querySelectorAll('body > form:nth-child(7) > div.wrapper.theme_lightblue > div.detail_center.clearfix > div.detail_main.clearfix > div.detail_top.clearfix');
            var item, position;
            if (odinfo && odinfo.length) {
            //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (maininfoSize && maininfoSize.length) {
        //返回有数据
        //设置phantomjs截图位置
            page.clipRect = computeSize(page, maininfoSize, 'normal');
            if (page.clipRect.height != 0) {
            //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'maininfo_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save maininfo',consoleData);
            } else {
            //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'maininfo_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save maininfo',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'maininfo_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save maininfo ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'stateside':function stateside(page, dataInfo) {
        console.log('start stateside() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息
        var statesideSize = page.evaluate(function () {
            var select = [];
            var odinfo = document.querySelectorAll('.state_side');
            //var odinfo= document.querySelectorAll('body > form:nth-child(7) > div.wrapper.theme_lightblue > div.detail_center.clearfix > div.detail_main.clearfix > div.detail_top.clearfix');
            var item, position;
            if (odinfo && odinfo.length) {
            //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (statesideSize && statesideSize.length) {
        //返回有数据
        //设置phantomjs截图位置
            page.clipRect = computeSize(page, statesideSize, 'normal');
            if (page.clipRect.height != 0) {
            //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'stateside_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save stateside',consoleData);
            } else {
            //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'stateside_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save stateside',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'stateside_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save stateside ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'relationorder':function relationorder(page, dataInfo) {
        console.log('start relationorder() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var relationorderSize = page.evaluate(function () {
            var clickItem = document.getElementById('tabRelationOrderInfo')[0];
            clickItem.click();
            var select = [];
            var odinfo = document.querySelectorAll('.state_side');

            var item, position;
            if (odinfo && odinfo.length) {
            //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (relationorderSize && relationorderSize.length) {
        //返回有数据
        //设置phantomjs截图位置
            page.clipRect = computeSize(page, relationorderSize, 'normal');
            if (page.clipRect.height != 0) {
            //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'relationorder_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save relationorder',consoleData);
            } else {
            //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'relationorder_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save relationorder',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'relationorder_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save relationorder ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'tabUserRecordsInfo':function tabUserRecordsInfo(page, dataInfo) {
        console.log('start tabUserRecordsInfo() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var tabUserRecordsInfoSize = page.evaluate(function () {

            var clickItem1 = document.getElementById('tabUserRecordsInfo');
            clickItem1.click();

            var select = [];
            var odinfo = document.querySelectorAll('.state_side');

            var item, position;
            if (odinfo && odinfo.length) {
            //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (tabUserRecordsInfoSize && tabUserRecordsInfoSize.length) {
        //返回有数据
        //设置phantomjs截图位置
            page.clipRect = computeSize(page, tabUserRecordsInfoSize, 'normal');
            if (page.clipRect.height != 0) {
            //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'tabUserRecordsInfo_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save tabUserRecordsInfo',consoleData);
            } else {
            //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'tabUserRecordsInfo_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save tabUserRecordsInfo',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'tabUserRecordsInfo_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save tabUserRecordsInfo ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'tripRecordTitle':function tripRecordTitle(page, dataInfo) {
        console.log('start tripRecordTitle() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var tripRecordTitleSize = page.evaluate(function () {

            var select = [];
            var odinfo = document.querySelectorAll('#detail_flightContent > table:nth-child(1)');

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (tripRecordTitleSize && tripRecordTitleSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, tripRecordTitleSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'tripRecordTitle_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save tripRecordTitle',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'tripRecordTitle_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save tripRecordTitle',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'tripRecordTitle_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save tripRecordTitle ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'flightContenthead':function flightContenthead(page, dataInfo) {
        console.log('start flightContenthead() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var flightContentheadSize = page.evaluate(function () {

            var select = [];
            var odinfo = document.querySelectorAll('#detail_flightContent > h3');

            var item, position;
            if (odinfo && odinfo.length) {
            //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (flightContentheadSize && flightContentheadSize.length) {
        //返回有数据
        //设置phantomjs截图位置
            page.clipRect = computeSize(page, flightContentheadSize, 'normal');
            if (page.clipRect.height != 0) {
            //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'flightContenthead_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save flightContenthead',consoleData);
            } else {
            //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'flightContenthead_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save flightContenthead',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'flightContenthead_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save flightContenthead ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'flightContent':function flightContent(page, dataInfo) {
        console.log('start flightContent() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var flightContentSize = page.evaluate(function () {

            var select = [];
            var odinfo = document.querySelectorAll('#detail_flightContent > table:nth-child(3)');

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (flightContentSize && flightContentSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, flightContentSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'flightContent_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save flightContent',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'flightContent_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save flightContent',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'flightContent_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save flightContent ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'cyzl':function cyzl(page, dataInfo) {
        console.log('start cyzl() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var cyzlSize = page.evaluate(function () {

            var select = [];
            $('#divZL').show();
            var odinfo = $('#divZL');

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
            
        });
        console.log(cyzlSize);
        if (cyzlSize && cyzlSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, cyzlSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'cyzl_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save cyzl',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'cyzl_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save cyzl',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'cyzl_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save cyzl ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'passenger':function passenger(page, dataInfo) {
        console.log('start passenger() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var passengerSize = page.evaluate(function () {

            var select = [];
            var odinfo = document.querySelectorAll('#detail_flightContent > table:nth-child(4)');
            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
            
        });
        console.log(passengerSize);
        if (passengerSize && passengerSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, passengerSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'passenger_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save passenger',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'passenger_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save passenger',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'passenger_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save passenger ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'noshow':function noshow(page, dataInfo) {
        console.log('start noshow() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var noshowSize = page.evaluate(function () {

            var select = [];
            $('#detail_flightContent > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(8) > span:nth-child(1)').click();
            var odinfo = $('#jquery_jmpInfo > div');
            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
            
        });
        console.log(noshowSize);
        if (noshowSize && noshowSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, noshowSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'noshow_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save noshow',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'noshow_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save noshow',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'noshow_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save noshow ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'noshow2':function noshow2(page, dataInfo) {
        console.log('start noshow2() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var noshow2Size = page.evaluate(function () {

            var select = [];
            try {$('#detail_flightContent > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(8) > span:nth-child(3)').click();
                var odinfo = $('#jquery_jmpInfo > div');}
            catch(e){}

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        console.log(noshow2Size);
        if (noshow2Size && noshow2Size.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, noshow2Size, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'noshow2_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save noshow2',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'noshow2_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save noshow2',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'noshow2_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save noshow2 ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'ticketmark':function ticketmark(page, dataInfo) {
        console.log('start ticketmark() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var ticketmarkSize = page.evaluate(function () {

            var select = [];
            try {$('#detail_flightContent > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(9) > a').click();
                var odinfo = $('#jquery_jmpInfo > div');}
            catch(e){}

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        console.log(ticketmarkSize);
        if (ticketmarkSize && ticketmarkSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, ticketmarkSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'ticketmark_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save ticketmark',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'ticketmark_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save ticketmark',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'ticketmark_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save ticketmark ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'Insurancedetail':function Insurancedetail(page, dataInfo) {
        console.log('start Insurancedetail() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var InsurancedetailSize = page.evaluate(function () {

            var select = [];
            try {$('#insuranceContent').click();
                var odinfo = $('#divInsuranceContent');}
            catch(e){}

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        console.log(InsurancedetailSize);
        if (InsurancedetailSize && InsurancedetailSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, InsurancedetailSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'Insurancedetail_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save Insurancedetail',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'Insurancedetail_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save Insurancedetail',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'Insurancedetail_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save Insurancedetail ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'reibursement':function reibursement(page, dataInfo) {
        console.log('start reibursement() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var reibursementSize = page.evaluate(function () {

            var select = [];
            var odinfo = $('#divInsuranceContent');

            var item, position;
            if (odinfo && odinfo.length) {
            //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                //位置信息
                    position: position,

                });
            }
            return select;
        });
        console.log(reibursementSize);
        if (reibursementSize && reibursementSize.length) {
        //返回有数据
        //设置phantomjs截图位置
            page.clipRect = computeSize(page, reibursementSize, 'normal');
            if (page.clipRect.height != 0) {
            //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'reibursement_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save reibursement',consoleData);
            } else {
            //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'reibursement_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save reibursement',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'reibursement_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save reibursement ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'PNR':function PNR(page, dataInfo) {
        console.log('start PNR() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var PNRSize = page.evaluate(function () {

            var select = [];
            try {$('#PNRBtn').click();
                var odinfo = $('#PNRPanel');}
            catch(e){}

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        console.log(PNRSize);
        if (PNRSize && PNRSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, PNRSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'PNR_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save PNR',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'PNR_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save PNR',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'PNR_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save PNR ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'message':function message(page, dataInfo) {
        console.log('start message() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var messageSize = page.evaluate(function () {

            var select = [];
            try {$('#MessageInfoBtn').click();
                var odinfo = $('#MessageInfoPanel');}
            catch(e){}

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        console.log(messageSize);
        if (messageSize && messageSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, messageSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'message_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save message',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'message_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save message',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'message_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save message ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'flighttool':function flighttool(page, dataInfo) {
        console.log('start flighttool() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var flighttoolSize = page.evaluate(function () {

            var select = [];
            try {$('#FlightToolBtn').click();
                var odinfo = $('#FlightToolPanel');}

            catch(e){}

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        console.log(flighttoolSize);
        if (flighttoolSize && flighttoolSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, flighttoolSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'flighttool_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save flighttool',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'flighttool_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save flighttool',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'flighttool_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save flighttool ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'xcartool':function xcartool(page, dataInfo) {
        console.log('start xcartool() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var xcartoolSize = page.evaluate(function () {

            var select = [];
            try {$('#XCarToolBtn').click();
                var odinfo = $('#XCarToolPanel');}

            catch(e){}

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        console.log(xcartoolSize);
        if (xcartoolSize && xcartoolSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, xcartoolSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'xcartool_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save xcartool',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'xcartool_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save xcartool',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'xcartool_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save xcartool ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'toolindemnity':function toolindemnity(page, dataInfo) {
        console.log('start toolindemnity() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var toolindemnitySize = page.evaluate(function () {

            var select = [];
            try { $('#ToolIndemnityBtn').click();
                var odinfo = $('#ToolIndemnityPanel');}

            catch(e){}

            var item, position;
            if (odinfo && odinfo.length) {
                //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });
            }
            return select;
        });
        console.log(toolindemnitySize);
        if (toolindemnitySize && toolindemnitySize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, toolindemnitySize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'toolindemnity_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save toolindemnity',consoleData);
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'toolindemnity_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save toolindemnity',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'toolindemnity_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save toolindemnity ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },

    'feedbacks':function feedbacks(page, dataInfo) {
        console.log('start feedbacks() useTime: ' + (Date.now() - page.settings.iniTime) / 5000 + 's');
        //优惠券信息

        var feedbacksSize = page.evaluate(function () {

            var select = [];
            try { $('#ToComplainBtn').click();
                var odinfo = $('#ToComplainPanel');}

            catch(e){}

            var item, position;
            if (odinfo && odinfo.length) {
            //获取需要截取模块的位置信息
                item = odinfo[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height,//|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    odinfo[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                //位置信息
                    position: position,

                });
            }
            return select;
        });
        console.log(feedbacksSize);
        if (feedbacksSize && feedbacksSize.length) {
        //返回有数据
        //设置phantomjs截图位置
            page.clipRect = computeSize(page, feedbacksSize, 'normal');
            if (page.clipRect.height != 0) {
            //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'feedbacks_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save feedbacks',consoleData);
            } else {
            //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'feedbacks_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save feedbacks',consoleData);
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'feedbacks_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save feedbacks ',consoleData);

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    }
};