/**
 * Created by lsyang on 2017-03-31 0031.
 */
var page = require('webpage').create(),
    //访问的url
    server =
        'http://flights.ctrip.fat3.qa.nt.ctripcorp.com/process/OrderDetail?Orderid=3026787533';
// 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3039177935'
// 'https://m.ctrip.fat4.qa.nt.ctripcorp.com/webapp/frequentflyer/card_manage.html'
// 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3761543993&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494512552649%2C%22endDate%22%3A1494512792649%2C%22pvid%22%3A%221032%22%2C%22sid%22%3A%2233%22%2C%22vid%22%3A%226F25D82029B511E786EE008EE6454B65%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2232001097410040727733%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
// 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3022304125'
// 'https://flight.mall.html5.fat128.qa.nt.ctripcorp.com/webapp/xbook/static/common/page/flight_xbook_h5/detailcoupon.html?pagetype=detailcoupon&popup=close&oid=3039067683&isHideNavBar=YES&GetUserInfos=00A3AAA07F'
// 'http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3020412540';
// 'https://flight.mall.html5.fat128.qa.nt.ctripcorp.com/webapp/xbook/static/common/page/flight_xbook_h5/detailcoupon.html?pagetype=detailcoupon&popup=close&id=0000&mock=1&isHideNavBar=YES';
// 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/xproduct/dest/crnweb/flight_xbook_h5/detailshuttle.html?pagetype=detailshuttle&productType=3&oid=3039160552&mock=1'/
// server ='https://flight.mall.html5.fat128.qa.nt.ctripcorp.com/webapp/xbook/static/common/page/flight_xbook_h5/detailcoupon.html?pagetype=detailcoupon&popup=close&oid=3022305768&isHideNavBar=YES&GetUserInfos=00A3AAA07F';

// 'https://m.ctrip.com/webapp/flight/xproduct/dest/crnweb/flight_xbook_h5/detailcoupon.html?pagetype=detailcoupon&popup=close&oid=3217039541&isHideNavBar=YES&sourcefrom=viewdetails'
//登录的cookie
var cookieLogin = '0qhbQpMZk+kd79FDSDoZa6rgT/qTuN/pgqQHMcaiTNFO9SfFwJHtVV3bOyC3r0KQf3NuaGanRgBBlESBJBvhk9A0RmFljBYCPH2mjtAPypKFujB1fU1TKJS9IQILQvC4iRYvCr1gJXxMfh+0a/5ykENpWh/i22tULcwkAlaFXAAwxwWxBjz4DQD5dfaMS7CK2PFDPQ7q/NnZez7jFtLE4PcyNBW5fbAVHtIXkTiYD6gnoAPOC/o+ApAEbnvmQn1t/iDzygUZ8o6FXMfpyA6T71chlB3U1k0XSll8g7GeOf3Lc5rplMvDcg==';
// '88B04A49413AFCBE9D1A973C8BBE22FE08091A3A63EA23C429BE393DE96F9BCF';
var cookieKey = 'ticket_ctrip';
var curTime = Date.now();
var utils = require('./utils');
var computeSize = utils.computeSize;
var loggerInfo = utils.loggerInfo;
var loggerTime = utils.loggerTime;
var loggerDebug = utils.loggerDebug;
var computeUseTime = utils.computeUseTime;
var consoleData = {};
var fs = require('fs');
if(cookieKey){
    phantom.addCookie({
        name: cookieKey,
        value: cookieLogin,
        domain: '.qa.nt.ctripcorp.com',//'.ctrip.com',  测试 .qa.nt.ctripcorp.com
        path: '/',
        secure: false,
        httponly: false,
        expires: curTime + (1000 * 60 * 60 * 24 * 5)
    });
}else{
    phantom.addCookie({
        name: 'cticket',
        value: cookieLogin,
        domain: '.qa.nt.ctripcorp.com',//'.ctrip.com',  测试 .qa.nt.ctripcorp.com
        path: '/',
        secure: false,
        httponly: false,
        expires: curTime + (1000 * 60 * 60 * 24 * 5)
    });
}

page.viewportSize ={
    width: 800,
    height: 2015
};
var dataInfo = {
    env: 'fat396',//环境，这边直接指定的测试数据，实际由外部传入
    orderid: '2185726380',//订单号，这边直接指定的测试数据，实际由外部传入
    PAGEPATH: 'D:/ImagePage/',//图片保存路径
    orderidIndex: 1,//订单个数，这边直接指定的测试数据，实际由外部传入
};
page.settings.index = 0;
page.settings.pageCount = 0;
page.settings.userAgent = 'WebKit/534.46 Mobile/9A405 Safari/7534.48.3';
page.settings.iniTime = Date.now();
page.orderid = dataInfo.orderid;
page.env = dataInfo.env;

Object.defineProperty(console, 'toFile', {
    get: function () {
        return console.__file__;
    },
    set: function (val) {
        if (!console.__file__ && val) {
            console.__log__ = console.log;
            console.log = function () {

                var msg = '';
                for (var i = 0; i < arguments.length; i++) {
                    msg += ((i === 0) ? '' : ' ') + arguments[i];
                }
                if (msg) {
                    fs.write(console.__file__, msg + '\r\n', 'a');
                }
            };
        }
        else if (console.__file__ && !val) {
            console.log = console.__log__;
        }
        console.__file__ = val;
    }
});


// var logPath = inputData.LOGPATH;
//重定向console到本地
//console.toFile = logPath + '/phantomjsLog_' + inputData.datekey + '.txt';
//恢复到控制台
console.toFile = '';

//要验证的函数
function hybridOrderState (page, dataInfo) {
    console.log('PAGEPATH' + dataInfo.PAGEPATH);
    loggerTime('start hybridOrderState() ', page.settings.iniTime);
    //启动的phantomjs中运行js脚本
    var orderStateSize = page.evaluate(function () {
        window.hybridFindStart = new Date().getTime();
        //查找订单状态模块
        var orderState = $('.order-state');
        var select = [];
        if ($ && orderState && orderState.length) {
            //模块存在
            //查找广告模块，设置全局变量
            var guanggao = window.guanggao || $('#js_ads');
            window.guanggao = guanggao;
            //隐藏广告模块
            guanggao.hide();
            //隐藏航司log
            var hangbanLogo = window.hangbanLogo || $('.flight-section-container .airline-icon');
            window.hangbanLogo = hangbanLogo;
            hangbanLogo.hide();
            //查找订单状态的位置
            var item = orderState;
            var position = item.offset();
            //记录运行时间，可忽略
            window.hybridFindEnd = new Date().getTime();
            //保存要返回的数据
            select.push({
                //位置信息
                position: position,
                //元素信息
                item: item,
                //时间信息，可以忽略
                time: window.hybridFindEnd - window.hybridFindStart
            });

        }
        return select;
    });
    //打印耗时，可以忽略
    loggerTime('findorderStateSizeDiv ', page.settings.iniTime);
    if (orderStateSize.length) {
        //返回有数据
        //设置phantomjs截图位置
        page.clipRect = computeSize(page, orderStateSize, 'normal');
        if (page.clipRect.height != 0) {
            //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
            page.render(dataInfo.PAGEPATH + 'hybridOrderState_success.png', {
                format: 'png',
                quality: '50'
            });
            //计算截图模块的页面个数，成功时+1，必须统计节点
            page.settings.pageCount++;
            //需要打印的信息，用于页面显示
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
            loggerInfo('success:  save hybridOrderState ',consoleData);

        } else {
            //模块高度为0，该div未显示
            page.clipRect = computeSize(page, [], 'whole');
            //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
            page.render(dataInfo.PAGEPATH + 'hybridOrderState_fail.png', {
                format: 'png',
                quality: '50'
            });
            //需要打印的信息，用于页面显示
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };

            loggerInfo('fail:  save hybridOrderState ',consoleData);

        }
    } else {

        page.clipRect = computeSize(page, [], 'whole');
        page.render(dataInfo.PAGEPATH + 'hybridOrderState_fail.png', {
            format: 'png',
            quality: '50'
        });
        consoleData = {
            env: dataInfo.env,
            orderid: dataInfo.orderid,
            number: dataInfo.orderidIndex,
        };
        loggerInfo('fail:  save hybridOrderState ',consoleData);

    }

    consoleData = {
        pageCount: page.settings.pageCount,
        useTime: computeUseTime(Date.now(), page.settings.iniTime),
    };
    loggerDebug(' ',consoleData);

}


function fftCardList(page, dataInfo){
    console.log('start');
    var fftCardListSize = page.evaluate(function () {
        var select = [];
        var cardList = document.querySelectorAll('.card-wrapper.card-wrapper-extra');
        // var cardList = $ && $('#viewport');
        // var body = document.body;
        // select.push(body)
        if (cardList && cardList.length) {
            //获取需要截取模块的位置信息
            // var item = cardList;
            // var position = item.offset();

            var item = cardList[0];
            var position = {
                top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                left: item.getBoundingClientRect().left,
                width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                height: item.getBoundingClientRect().height, //|| page.viewportSize.height////位置计算： 该点高度
            };


            //保存要返回的数据
            select.push({
                //位置信息
                position: position,
                //元素信息
                // item: item.html()
            });
            select.push(document.body);
        }else {
            select.push('222');
        }
        return select;
    });
    console.log('fftCardListSize:' + JSON.stringify(fftCardListSize));
    if (fftCardListSize.length) {
        //返回有数据
        //设置phantomjs截图位置
        console.log('save cardlist');
        page.clipRect = computeSize(page, fftCardListSize, 'normal');
        if (page.clipRect.height != 0) {
            //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
            page.render(dataInfo.PAGEPATH + 'cardList_success.png', {
                format: 'png',
                quality: '50'
            });
        }
    }
}

function fftCardDetail(page, dataInfo){
    console.log('start');
    var fftCardBtnSize = page.evaluate(function () {
        var select = [];
        var cardList = document.querySelectorAll('[data-speed="列表页：查看权益按钮"]');
        // var cardList = $ && $('#viewport');
        // var body = document.body;
        // select.push(body)
        if (cardList && cardList.length) {
            //获取需要截取模块的位置信息
            // var item = cardList;
            // var position = item.offset();

            var item = cardList[0];
            var position = {
                top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                left: item.getBoundingClientRect().left,
                width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                height: item.getBoundingClientRect().height, //|| page.viewportSize.height////位置计算： 该点高度
            };
            // cardList[0].click();

            //保存要返回的数据
            select.push({
                //位置信息
                position: position,
                //元素信息
                // item: item.html()
            });
            // var cardDetail = document.querySelectorAll('.card-wrapper');
            //
            //
            //
            //
            // // var cardList = $ && $('#viewport');
            // // var body = document.body;
            // // select.push(body)
            // if (cardDetail && cardDetail.length) {
            //     //获取需要截取模块的位置信息
            //     // var item = cardList;
            //     // var position = item.offset();
            //
            //     item = cardDetail[0];
            //     position = {
            //         top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
            //         left: item.getBoundingClientRect().left,
            //         width: item.getBoundingClientRect().width,//位置计算： 该点宽度
            //         height: item.getBoundingClientRect().height, //|| page.viewportSize.height////位置计算： 该点高度
            //     };
            //
            //
            //     //保存要返回的数据
            //     select.push({
            //         //位置信息
            //         position: position,
            //         //元素信息
            //         // item: item.html()
            //     });
            //     // select.push(document.body)
            // } else {
            //     select.push("222");
            // }
        }
        return select;
    });




    console.log('fftCardListSize:' + JSON.stringify(fftCardBtnSize));
    if (fftCardBtnSize.length) {
        //返回有数据
        //设置phantomjs截图位置
        console.log('save cardlist');
        page.clipRect = computeSize(page, fftCardBtnSize, 'normal');
        if (page.clipRect.height != 0) {
            //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
            page.render(dataInfo.PAGEPATH + 'cardList_success.png', {
                format: 'png',
                quality: '50'
            });
        }



        var taskCount = 20;
        var taskFn = function () {
            taskCount--;
            var pageResult = page.evaluate(function () {
                var select = [];
                var cardDetail = document.querySelectorAll('.card-wrapper');
                if (cardDetail && cardDetail.length) {
                    //获取需要截取模块的位置信息
                    // var item = cardList;
                    // var position = item.offset();

                    var item = cardDetail[0];
                    var position = {
                        top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                        left: item.getBoundingClientRect().left,
                        width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                        height: item.getBoundingClientRect().height, //|| page.viewportSize.height////位置计算： 该点高度
                    };


                    //保存要返回的数据
                    select.push({
                        //位置信息
                        position: position,
                        //元素信息
                        // item: item.html()
                    });
                    // select.push(document.body)
                }

                return select;
                // || ($ && $('.hh-title').length > 0)
            });
            if (!taskCount) {
                // console.log('fail: task end, exit app. Please try again');
                //
                page.clipRect = computeSize(page, pageResult[0], 'whole');
                page.render(dataInfo.PAGEPATH + 'cardDetail_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                // //callback(null, 'fail: login');
                // phantom.exit();

            }
            else {
                if (pageResult && pageResult.length ) {
                    //console.log('success: open login url');
                    clearTimeout(timer);
                    if (pageResult && pageResult.length) {
                        //返回有数据
                        //设置phantomjs截图位置
                        console.log('save cardDetail_success');
                        page.clipRect = computeSize(page, pageResult[0], 'normal');
                        if (page.clipRect.height != 0) {
                            //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                            page.render(dataInfo.PAGEPATH + 'cardDetail_success.png', {
                                format: 'png',
                                quality: '50'
                            });
                        }
                    }
                }
                else {
                    console.log('wait: for login');
                    timer = setTimeout(taskFn, 1000);
                }
            }
        };
        var timer = setTimeout(taskFn, 1000);


        // setTimeout(function(){
        //     // var fftCardDetailSize = page.evaluate(function () {
        //     //     var select = [];
        //     //     var cardDetail = document.querySelectorAll('.card-wrapper');
        //     //     // var cardList = $ && $('#viewport');
        //     //     // var body = document.body;
        //     //     // select.push(body)
        //     //     if (cardDetail && cardDetail.length) {
        //     //         //获取需要截取模块的位置信息
        //     //         // var item = cardList;
        //     //         // var position = item.offset();
        //     //
        //     //         var item = cardDetail[0];
        //     //         var position = {
        //     //             top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
        //     //             left: item.getBoundingClientRect().left,
        //     //             width: item.getBoundingClientRect().width,//位置计算： 该点宽度
        //     //             height: item.getBoundingClientRect().height, //|| page.viewportSize.height////位置计算： 该点高度
        //     //         };
        //     //
        //     //
        //     //         //保存要返回的数据
        //     //         select.push({
        //     //             //位置信息
        //     //             position: position,
        //     //             //元素信息
        //     //             // item: item.html()
        //     //         });
        //     //         // select.push(document.body)
        //     //     }else {
        //     //         select.push("222");
        //     //     }
        //     //     return select;
        //     // });
        //
        //     if (fftCardBtnSize && fftCardBtnSize.length) {
        //         //返回有数据
        //         //设置phantomjs截图位置
        //         console.log('save cardDetail_success');
        //         page.clipRect = computeSize(page, fftCardBtnSize[1], 'normal');
        //         if (page.clipRect.height != 0) {
        //             //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
        //             page.render(dataInfo.PAGEPATH + 'cardDetail_success.png', {
        //                 format: 'png',
        //                 quality: '50'
        //             });
        //         }
        //     }
        //
        //
        // },1000*3)
    }
}


function orderDetailWhole(page, dataInfo){

    // js_ads
    var orderDetailSize = page.evaluate(function () {
        var select = [];
        var orderDetailCon = $ && $('.j-orderdetail-container');
        var guanggao = $ && $('#js_ads');
        // var cardList = $ && $('#viewport');
        // var body = document.body;
        // select.push(body)
        if (orderDetailCon && orderDetailCon.length

        && guanggao && guanggao.length) {
            //获取需要截取模块的位置信息
            var item = orderDetailCon;
            var position = item.offset();
            //保存要返回的数据
            select.push({
                //位置信息
                position: position,
                //元素信息
                // item: item.html()
            });
            item = guanggao;
            position = item.offset();
            //保存要返回的数据
            select.push({
                //位置信息
                position: position,
                //元素信息
                // item: item.html()
            });

        }else {
            select.push('222');
        }
        return select;
    });
    if(orderDetailSize && orderDetailSize.length){
        console.log('save cardlist');
        page.clipRect = computeSize(page, orderDetailSize, 'subTwoTop');
        if (page.clipRect.height != 0) {
            //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
            page.render(dataInfo.PAGEPATH + 'orderDetail_success.png', {
                format: 'png',
                quality: '50'
            });
        }

    }

}



page.open(server,  function (status) {
    if (status !== 'success') {
        console.log('Unable to post!');
        page.render(dataInfo.PAGEPATH + 'fail.png', {
            format: 'png',
            quality: '50'
        });
        page.close();
    } else {
        console.log('success');
        // 延时执行，时间可以自己调整
        setTimeout(function () {
            hybridOrderState (page, dataInfo);
        }, 1000*15);
        //截取页面整图，时间可以自己调整
        setTimeout(function () {
            console.log('save page');
            page.render(dataInfo.PAGEPATH + 'fftsuccess.png',{
                format: 'png',
                quality: '50'
            });
        }, 1000*10);
    }

});
