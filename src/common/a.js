/**
 * Created by lsyang on 2017-05-13 0013.
 */


const phantom = require('phantom');

function sleep(time) {
    return new Promise(resolve =>　{
        setTimeout(resolve, time);
    });
}

(async function() {
    const instance = await phantom.create();
    const page = await instance.createPage();

    await page.addCookie({
        name: 'cticket',
        value: '88B04A49413AFCBE9D1A973C8BBE22FEB34AF6992148A59486BE979C8BF18610',
        domain: '.qa.nt.ctripcorp.com',
        path: '/',
        secure: false,
        httponly: false,
        expires: new Date().getTime() + (1000 * 60 * 60 * 24 * 5)
    });

    page.setting('clearMemoryCaches', true);
    page.property('onResourceRequested', function(requestData, networkRequest) {
        // console.log(requestData.url);
        var hostname = requestData.url.match(/\/\/.+?\//)[0];
        if (!/ctrip/.test(hostname) || /ubt/.test(hostname) || /socket/.test(hostname) || /pages.dev.sh.ctriptravel.com/.test(hostname)) {
            //pages.dev.sh.ctriptravel.com 会有配置文件超时影响详情loading
            // console.log(page.settings.index + 'req Aborting: ' + JSON.stringify(requestData, undefined, 4));
            networkRequest.abort();
        }else{
            // console.log(requestData.url);
        }
    });


    //
    let outObj = instance.createOutObject();
    //
    // await page.property('onResourceReceived', function(response, out) {
    //     out.lastResponse = response;
    // }, outObj);
    //
    // // await page.open(`http://localhost:${port}/test`);
    //
    // let lastResponse = await outObj.property('lastResponse');
    // await console.log(lastResponse.url);
    // //
    // var outObj = instance.createOutObject();
    //
    // await page.property('onResourceReceived', function(res, out) {
    //     // console.log(res.url)
    //
    //     if (res.url && res.stage !== 'start' && res.contentType != null) {
    //
    //
    //
    //         if(/OrderDetailSearchV2\?/.test(res.url) && (res.contentType =='application/json;charset=utf-8')){
    //             console.log('--------------------'+JSON.stringify(res));
    //             out.res = res;
    //             lastResponse = res;
    //             // out.flag = true;
    //             // console.log('---'+outObj.flag)
    //         }
    //
    //
    //     }
    //
    // }, outObj);


    // let flag =  await outObj.property('flag');
    // await console.log(JSON.stringify(flag));
    // if(flag){
    //     console.log(flag)
    //     //
    //     // setTimeout(function(){
    //     //     console.log(flag)
    //     //     console.log('save page' );
    //     //     page.render('ctrip111' + '_' +  '.png');
    //     //
    //     //     page.close();
    //     // }, 6*1000);
    //
    //
    //
    //     await sleep(10*1000);
    //     await page.render('ctrip.png');
    //     // await page.close();
    // }




    await page.property('viewportSize', {width: 540, height: 6000});

    // await page.on("onResourceRequested", function(requestData) {
    //     // if (/OrderDetailSearchV2/.test(requestData.url)) {
    //     //
    //     // }
    //     //console.info('Requesting', requestData.url)
    // });
    var url = 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3827035615&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494643110791%2C%22endDate%22%3A1494643350791%2C%22pvid%22%3A%222934%22%2C%22sid%22%3A%2273%22%2C%22vid%22%3A%2299250E4A17C346C0886E646E190DEBDF%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2212001044310024263447%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D';

    //outObj.urls = [];
    // let page = await phantom.createPage();
    //var urls = [];
    var flag = false;
    // page.on('onResourceReceived', function(res, out) {
    //     // lastResponse.push(res)
    //     if (res.url && res.stage !== 'start' && res.contentType != null) {
    //         //
    //         if (/OrderDetailSearchV2\?/.test(res.url) && (res.contentType == 'application/json;charset=utf-8')) {
    //             console.log(res.url + '******stage: '+res.stage + '***res.status: '+ res.status);
    //             console.log('*********************************************'+flag)
    //             //flag = true;
    //             console.log('*********************************************'+flag)
    //             // function sleep(time) {
    //             //     return new Promise(resolve =>　{
    //             //         setTimeout(resolve, time);
    //             //     });
    //             // }
    //             // await sleep(10*1000);
    //              page.render('ctrip1.png');
    //             setTimeout(function(){
    //                 page.render('ctrip.png');
    //                 console.log('save ')
    //             }, 20*1000);
    //
    //
    //         }
    //     }
    //    // urls.push(res.url);
    //     // console.log('---'+res.url);
    //
    //
    //     //     }
    //     // }
    // });
    await page.open(url);
    // let lastResponse = await instance.windowProperty('lastResponse');
    //
    // const status = await page.open(url);
    // let res = await outObj.property('res');
    // let lastResponse = instance.windowProperty('lastResponse');
    // await console.log(lastResponse);
    // await console.log('---'+JSON.stringify(res));

    // await let urls = outObj.property('urls').
    //  urls.forEach(function(urls){
    //     console.log(urls);
    // });

    await sleep(10*1000);
    await page.render('ctrip.png');

    // console.log(`Page opened with status [${status}].`);



    // await instance.exit();
}());