/**
 * Created by lsyang on 2017-05-11 0011.
 */

// var page = require('webpage').create();
var log4js = require('log4js');
log4js.configure({
    appenders: [
        { type: 'console' },
        { type: 'file', filename: './job.log', category: 'logHttp' }
    ],
    replaceConsole: true
});
var logger = log4js.getLogger('logHttp');

var requestData = {
    pageid: '10320608519',
    count: 1,
    ubtKeys: [
        {
            key: 'custom_key',
            value: '_flight_hybird_ctrip'
        }
    ],
    dataKeys: [{
        key:'oid',
        path:['order','oid']
    }]
};
var async = require('async');
var data = 'oid';
var getData = require('../postService/hybridDetail/getData.js');

getData.getUrl(requestData, data).then(urls =>{

    logger.info(JSON.stringify(urls));
    var i = 0;
    async.mapLimit(urls, 1, function (url, callback){
        i++;
        start(url,i, callback);
    },function(err,result){

        for (var item in result) {
            logger.info(result[item]);
        }

        // phInstance.exit();

    });
});
//
//
// const phantom = require('phantom');
//
// (async function() {
//     const instance = await phantom.create();
//     const page = await instance.createPage();
//     await page.on("onResourceRequested", function(requestData) {
//         console.info('Requesting', requestData.url)
//     });
//
//     const status = await page.open('https://stackoverflow.com/');
//     logger.info(status);
//
//     const content = await page.property('content');
//     logger.info(content);
//
//     await instance.exit();
// }());

// var url = 'http://m.ctrip.fat397.qa.nt.ctripcorp.com:9000/detailcoupon?pagetype=detailcoupon&popup=close&isHideNavBar=YES&productType=5&oid=3039143867'
// url = 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3761543993&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494512552649%2C%22endDate%22%3A1494512792649%2C%22pvid%22%3A%221032%22%2C%22sid%22%3A%2233%22%2C%22vid%22%3A%226F25D82029B511E786EE008EE6454B65%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2232001097410040727733%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
// url = 'http://flight.mall.html5.fat128.qa.nt.ctripcorp.com/webapp/xbook/static/common/page/flight_xbook_h5/detailcoupon.html?pagetype=detailcoupon&popup=close&isHideNavBar=YES&productType=5&oid=3039143867&mock=1'
var phInstance = null;

// 每个页面的任务控制器

async function start(url, i, callback){
    const phantom = require('phantom');

    var sitepage = null;

    phantom.create()
        .then(instance => {
            phInstance = instance;
            return instance.createPage();
        })
        .then(async page => {

            var arrRes = [
                {
                    //依赖的url
                    url: 'OrderDetailSearchV2?',
                    //url对应的正则
                    urlRe: /OrderDetailSearchV2\?/,
                    //是否发起请求，暂时未用该数据
                    isReqSucc: false,
                    //是否接收到返回 默认false
                    isResSucc: false
                }];
            page.setting('clearMemoryCaches', true);
            page.property('onResourceRequested', function(requestData, networkRequest) {
                // console.log(requestData.url);
                var hostname = requestData.url.match(/\/\/.+?\//)[0];
                if (!/ctrip/.test(hostname) || /ubt/.test(hostname) || /socket/.test(hostname) || /pages.dev.sh.ctriptravel.com/.test(hostname)) {
                    //pages.dev.sh.ctriptravel.com 会有配置文件超时影响详情loading
                    // console.log(page.settings.index + 'req Aborting: ' + JSON.stringify(requestData, undefined, 4));
                    networkRequest.abort();
                }else{
                    // console.log(requestData.url);
                }
            });
            //
            var outObj = phInstance.createOutObject();
            // outObj.flag = false;
            await page.property('onResourceReceived', function(res, outObj) {

                if (res.url && res.stage !== 'start' && res.contentType != null) {



                    if(/OrderDetailSearchV2\?/.test(res.url) && (res.contentType =='application/json;charset=utf-8')){
                        console.log('--------------------'+JSON.stringify(res));
                        outObj.flag = true;
                    }

                    // function pageTask(page, arrRes, res,i, callback) {
                    //
                    //
                    //     var flag = true;
                    //     arrRes.forEach(function (item) {
                    //
                    //         if(!item.isResSucc && item.urlRe.test(res.url) && (res.contentType =='application/json;charset=utf-8' || res.contentType === item.resType)){
                    //             item.isResSucc = item.urlRe.test(res.url); //&& res.status =='200';
                    //             // console.log('received: ' + JSON.stringify(res, undefined, 4));
                    //
                    //
                    //             console.log('received:' + res.url);
                    //             console.log('******'+flag)
                    //
                    //             arrRes.forEach(function (item) {
                    //
                    //                 if (!item.isResSucc) flag = false;
                    //             });
                    //             //
                    //             if (flag) {
                    //
                    //                 //延时1s后执
                    //                 setTimeout(function(){
                    //                     console.log('save page' + i);
                    //                     page.render('ctrip' + i + '_' +  '.png');
                    //                     callback(null, 'page' + i);
                    //                     page.close();
                    //                 }, 6*1000);
                    //
                    //             }
                    //         }
                    //
                    //     });
                    // }

                    // console.log('res:'S+res.url);
                    // pageTask(page, arrRes, res,i, callback);
                }

            }, outObj);

            let flag =  await outObj.property('flag');

            if(flag){

                setTimeout(function(){
                    console.log(flag);
                    console.log('save page' );
                    page.render('ctrip111' + '_' +  '.png');
                    callback(null, 'page' );
                    page.close();
                }, 6*1000);
            }


            //添加后记录了js的日志
            // page.clearMemoryCache();

            // use page
            // page.addCookie({
            //     name: 'cticket',
            //     value: '88B04A49413AFCBE9D1A973C8BBE22FEB34AF6992148A59486BE979C8BF18610',
            //     domain: '.qa.nt.ctripcorp.com',
            //     path: '/',
            //     secure: false,
            //     httponly: false,
            //     expires: new Date().getTime() + (1000 * 60 * 60 * 24 * 5)
            // })
            page.property('viewportSize', { width: 414, height: 3000 }).then(function() {
                console.log(i);
                // page.settings.clearMemoryCaches = true;
                //添加后记录了js的日志
                // page.clearMemoryCache();
                // url='http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=2530913164&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494642253299%2C%22endDate%22%3A1494642493299%2C%22pvid%22%3A%223217%22%2C%22sid%22%3A%22153%22%2C%22vid%22%3A%220E0159037CE74A42BB7C4C787B167F5E%22%7D%7D%2C%22proxyUrl%22proxyUrl2%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2212001082210029673677%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
                // url='http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3824983035&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494642251141%2C%22endDate%22%3A1494642491141%2C%22pvid%22%3A%2273%22%2C%22sid%22%3A%224%22%2C%22vid%22%3A%221A45A0FEF473492598150AEFD09609D5%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2212001059110041832603%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
                // url = 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3827032553&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494642257344%2C%22endDate%22%3A1494642497344%2C%22pvid%22%3A%222915%22%2C%22sid%22%3A%2259%22%2C%22vid%22%3A%22B29412FCEC374F7291704AF8B98B2634%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2212001052510031012248%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
                page.open(url);


            });



        })
        .catch(error => {
            console.log(error);
            phInstance.exit();
        });

}


//
// (async function() {
//     const instance = await phantom.create();
//     const page = await instance.createPage();
//
//     await page.addCookie({
//         name: 'cticket',
//         value: '88B04A49413AFCBE9D1A973C8BBE22FEB34AF6992148A59486BE979C8BF18610',
//         domain: '.qa.nt.ctripcorp.com',
//         path: '/',
//         secure: false,
//         httponly: false,
//         expires: new Date().getTime() + (1000 * 60 * 60 * 24 * 5)
//     });
//
//     await page.property('viewportSize', {width: 540, height: 6000});
//
//     // await page.on("onResourceRequested", function(requestData) {
//     //     // if (/OrderDetailSearchV2/.test(requestData.url)) {
//     //     //
//     //     // }
//     //     //console.info('Requesting', requestData.url)
//     // });
//
//     const status = await page.open('http://m.ctrip.fat397.qa.nt.ctripcorp.com:9000/detailcoupon?pagetype=detailcoupon&popup=close&isHideNavBar=YES&productType=5&oid=3039143867');
//     page.render('ctrip.png');
//     logger.info(`Page opened with status [${status}].`);
//
//
//
//     await instance.exit();
// }());