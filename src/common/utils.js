//计算div，计算截图尺寸及位置
Array.max = function( array ){
    return Math.max.apply( Math, array );
};
Array.min = function( array ){
    return Math.min.apply( Math, array );
};

Array.prototype.sum = function (){
    return this.reduce(function (partial, value){
        return partial + value;
    });
};

module.exports = {
    //根据输入的div,设置截图的位置
    computeSize: function(page, findSize, type) {
        console.log('type:'+ type);
        var data = {};
        var topData = [];
        var heightData = [];
        var widthData = [];
        var leftData = [];
        if(type=='add'){
            findSize.forEach(function(item){

                if(item.position){
                    console.log('item.position:'+JSON.stringify(item.position));
                    topData.push(item.position.top);
                    heightData.push(item.position.height);
                    widthData.push(item.position.width);
                    leftData.push(item.position.left);
                }
            });
            data = {
                top: Array.min(topData),//距离顶部最小的top
                left: Array.min(leftData),//距离左侧最小left
                width: Array.max(widthData),//最大的宽度
                height: heightData.sum()//所有高度之和
            };
        }
        if(findSize && findSize[0] && findSize[0].position){

            switch (type) {

            case 'normal':
                data = {
                    //位置计算： 该点距离顶部的位置
                    top: findSize[0].position.top,//位置计算： 该点距离顶部的位置
                    left: findSize[0].position.left,
                    width: findSize[0].position.width,//位置计算： 该点宽度
                    height: (findSize[0].position.height), //|| page.viewportSize.height////位置计算： 该点高度
                };
                break;

            case 'normalWithoutLeft':
                data = {
                    //位置计算： 该点距离顶部的位置
                    top: findSize[0].position.top,//位置计算： 该点距离顶部的位置
                    left: 0,
                    width: findSize[0].position.width,//位置计算： 该点宽度
                    height: (findSize[0].position.height), //|| page.viewportSize.height////位置计算： 该点高度
                };
                break;
            case 'sub':
                //距离差
                data = {
                    //位置计算： 该点距离顶部的位置
                    top: findSize[0].position.top,//位置计算： 该点距离顶部的位置
                    left: 0,
                    width: findSize[0].position.width,//位置计算： 该点宽度
                    height: (findSize[0].position.height - findSize[1].position.height - findSize[2].position.height), //|| page.viewportSize.height////位置计算： 该点高度
                };
                break;
            case 'whole':
                //全屏尺寸
                data = {
                    top: 0,
                    left: 0,
                    width: page.viewportSize.width,
                    height: page.viewportSize.height
                };
                break;

            case 'rbk':
                data = {
                    top:　findSize[0].position.top,
                    left: 0,
                    width: findSize[0].position.width,
                    height: findSize[1].position.height - findSize[2].position.height,
                };
                break;
            case 'subTwoTop':
                data = {
                    //位置计算： 该点距离顶部的位置
                    top: findSize[0].position.top,//位置计算： 该点距离顶部的位置
                    left: 0,
                    width: findSize[0].position.width,//位置计算： 该点宽度
                    height: (findSize[1].position.top - findSize[0].position.top), //|| page.viewportSize.height////位置计算： 该点高度
                };
                break;
            case 'flightChange':
                data = {
                    //位置计算： 该点距离顶部的位置
                    top: findSize[0].position.top + findSize[1].position.height,//位置计算： 该点距离顶部的位置
                    left: 0,
                    width: findSize[0].position.width,//位置计算： 该点宽度
                    height: (findSize[0].position.height - findSize[1].position.height), //|| page.viewportSize.height////位置计算： 该点高度
                };
                break;
            }

        }else{
            console.log('result: not find the type');
            data = {
                top: 0,
                left: 0,
                width: page.viewportSize.width,
                height: page.viewportSize.height
            };
        }
        console.log(JSON.stringify(data));
        // console.log('top: ' + data.top +
        //     ' width: ' + data.width +
        //     ' height: ' + data.height);
        return data;
    },
    //打印调试用的log
    loggerDebug: function(info, data){
        console.log(info + JSON.stringify(data));
    },
    //打印页面展示用的log
    loggerInfo: function(info, data){
        //console.log(info  + JSON.stringify(data).replace(/[{"|"}]/g,''));
        console.log(info  + JSON.stringify(data).replace(/"/g,''));
    },
    //打印耗时用的log.可以不关注
    loggerTime: function(info, useTime){
        console.log(info + ' useTime: ' + (Date.now() - useTime) / 1000  +'s');
    },
    //计算耗时
    computeUseTime: function (time1, time2) {
        return (time1 - time2) /1000 +'s';
    },
    //回调函数log
    getCallbackInfo: function(info, data){
        return info + JSON.stringify(data).replace(/[{"|"}]/g,'');
    },
    clickItemByClassName: function(className, index){
        var clickEvent = document.createEvent('HTMLEvents');
        clickEvent.initEvent('click',false,true);
        var clickItem = document.getElementsByClassName(className)[index];
        clickItem.dispatchEvent(clickEvent);
        clickItem.click();
    },
};