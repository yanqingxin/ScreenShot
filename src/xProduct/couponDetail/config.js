
//截取模块依赖的接口 需监控的css、js

//需要截取的模块（div），依赖的接口
//公共模块
var commonMod = [
    {
        // //订单详情页依赖的url
        // url: 'OrderDetailSearchV2?',
        // //url对应的正则
        // urlRe: /OrderDetailSearchV2\?/,
        // //是否发起请求，暂时未用该数据
        // isReqSucc: false,
        // //是否接收到返回 默认false
        // isResSucc: false

        //优惠券详情页依赖的url
        url: 'getCouponOrderDetail?',
        //url对应的正则
        urlRe: /getCouponOrderDetail\?/,
        //是否发起请求，暂时未用该数据
        isReqSucc: false,
        //是否接收到返回 默认false
        isResSucc: false

    },
];

//各个模块对应所需判断的接口
var ajaxHash = {
    // //订单状态模块
    // 'hybridOrderState': [{
    //     url: 'OrderDetailSearchV2?',
    //     urlRe: /OrderDetailSearchV2\?/,
    //     isReqSucc: false,
    //     isResSucc: false
    // },
    // ],
    //优惠券状态 --优惠券页面暂时木有分模块
    'crnCouponDetail':[{
        url:'getCouponOrderDetail?',
        urlRe:/getCouponOrderDetail\?/,
        isReqSucc:false,
        isResSucc:false
    },
    ]
};

//需要监控的css
var cssData = [
    // /detail(\.min)?\.css/,
    // /detail\-base(\.min)?\.css/
];
//需要监控的js
var jsData = [
    // /common\.js/,
    // /orderdetail\-flight\.js/,
    // /lizard\.lite\.min\.js/
    /XDetailCoupon\.js/,
];
//添加公共模块  将公共模块和个模块需要的数据合并
for (var item in ajaxHash) {
    ajaxHash[item] = ajaxHash[item].concat(commonMod);
}

module.exports = {
    ajaxHash: ajaxHash,
    cssData: cssData,
    jsData: jsData
};