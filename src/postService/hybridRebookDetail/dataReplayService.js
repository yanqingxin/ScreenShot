/**
 * Created by lsyang on 2017-05-14 0014.
 */


//截图可选环境url
var url395 = 'http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid={Orderid}';
var url396 = 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid={Orderid}';
var _ = require('lodash');
//截图所需基础数据，必传
var dataService = {
    //截图所需url
    urls: [],
    //日志展示用的环境
    envs: [],
    //每个page存放路径
    pagefiles: [],
    //截图的订单号
    orderids: [],
    //截图的第几个订单号
    orderidIndexs: [],
    //log保存路径
    LOGPATH: '',
    //截图page保存路径
    PAGEPATH:'',
    //保存整图的时候，页面名称
    wholePageName: 'hybridRbkDetail',
};

function getBaseReplayData(inputData, result) {
    var pageFile1 = '';
    var pageFile2 = '';

    var urlHybridDetail1 = '';
    var urlHybridDetail2 = '';
    //设置log及图片保存位置  无需修改
    if(dataService  && dataService.LOGPATH ===''){
        dataService.LOGPATH = inputData.LOGPATH;
    }
    if(dataService &&  dataService.PAGEPATH ===''){
        dataService.PAGEPATH = inputData.PAGEPATH;
    }
    //根据可选环境，生成截图所需访问的urls,
    if (inputData.env1 == 'fat395') {
        urlHybridDetail1 = url395;
        pageFile1 = dataService.PAGEPATH + '-env1-' + inputData.env1 + '/';
    } else if (inputData.env1 == 'fat396') {
        urlHybridDetail1 = url396;
        pageFile1 = dataService.PAGEPATH + '-env1-' + inputData.env1 + '/';
    }
    if (inputData.env2 == 'fat395') {
        urlHybridDetail2 = url395;
        pageFile2 = dataService.PAGEPATH + '-env2-' + inputData.env2 + '/';
    } else if (inputData.env2 == 'fat396') {
        urlHybridDetail2 = url396;
        pageFile2 = dataService.PAGEPATH + '-env2-' + inputData.env2 + '/';
    }

    // for (var i = 0; i < inputData.orderid.length; i++) {
    //     var url = urlHybridDetail1.replace(/\{Orderid\}/, inputData.orderid[i]);
    //     dataService.urls.push(url);
    //     dataService.pagefiles.push(pageFile1);
    //     dataService.orderids.push(inputData.orderid[i]);
    //     dataService.envs.push(1 + " " + inputData.env1);
    //     //同一个订单号 算一个orderid,此处orderidindex同orderid保持一致，orderid相同的orderidIndex相同
    //     dataService.orderidIndexs.push(i + 1);
    //     url = urlHybridDetail2.replace(/\{Orderid\}/, inputData.orderid[i]);
    //     dataService.urls.push(url);
    //     dataService.pagefiles.push(pageFile2);
    //     dataService.orderids.push(inputData.orderid[i]);
    //     dataService.envs.push(2 + " " + inputData.env2);
    //     //同一个订单号 算一个orderid,此处orderidindex同orderid保持一致，orderid相同的orderidIndex相同
    //     dataService.orderidIndexs.push(i + 1);
    // }
    if(result && result.arr &&　result.data){
        result.arr.forEach(function (item, i) {
            const baseData = {

                'listMap': {
                    '/webapp/flight/orderdetail/orderdetail.html': {
                        'startDate': item.startDate,
                        'endDate': item.endDate,
                        'pvid': item.pvid,
                        'sid': item.sid,
                        'vid': item.vid
                    }
                },
                'proxyUrl': 'http://10.18.6.135:37016/proxies/',
                'clientID': item.clientID,
                'isFat': false,
                'type': item.type
            };
            var value = _.find(item.extraKeys, function (item) {
                return item.key === result.data;
            }).value;
            // var value = item.extraKeys.find(function(item){
            //     return item.key == result.data
            // }).value;
            const replay = encodeURIComponent(JSON.stringify(baseData));
            var orderid = value;
            dataService.pagefiles.push(pageFile1);
            var baseUrl = urlHybridDetail1.replace(/\{Orderid\}/, orderid);
            var url = baseUrl +'&replay=' + replay;
            // url = 'http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3787105441&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494839709794%2C%22endDate%22%3A1494839949794%2C%22pvid%22%3A%22945%22%2C%22sid%22%3A%2250%22%2C%22vid%22%3A%22B71C6E5C9E254EE5B848CCBD44C0E08B%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2212001175210025564216%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
            // url = 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=2537421694&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494652644830%2C%22endDate%22%3A1494652884830%2C%22pvid%22%3A%222632%22%2C%22sid%22%3A%2276%22%2C%22vid%22%3A%2207B7A5ED2F034D589D366A7D5C6E8C14%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2212001080510030548939%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
            // url ='http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3940116072&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1495123073133%2C%22endDate%22%3A1495123313133%2C%22pvid%22%3A%22454%22%2C%22sid%22%3A%2214%22%2C%22vid%22%3A%22DF0987F62DB84281BF871358C41DF2C3%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2243001159410041845018%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
            console.log(url);
            dataService.orderids.push(orderid);
            dataService.urls.push(url);
            dataService.envs.push(1 + ' ' + inputData.env1);
            //同一个订单号 算一个orderid,此处orderidindex同orderid保持一致，orderid相同的orderidIndex相同
            dataService.orderidIndexs.push(i + 1);
            baseUrl = urlHybridDetail2.replace(/\{Orderid\}/, orderid);
            url = baseUrl +'&replay=' + replay;
            // url = 'http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3787105441&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494839709794%2C%22endDate%22%3A1494839949794%2C%22pvid%22%3A%22945%22%2C%22sid%22%3A%2250%22%2C%22vid%22%3A%22B71C6E5C9E254EE5B848CCBD44C0E08B%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2212001175210025564216%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
            // url = 'http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=2537421694&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494652644830%2C%22endDate%22%3A1494652884830%2C%22pvid%22%3A%222632%22%2C%22sid%22%3A%2276%22%2C%22vid%22%3A%2207B7A5ED2F034D589D366A7D5C6E8C14%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2212001080510030548939%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
            // url ='http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3940116072&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1495123073133%2C%22endDate%22%3A1495123313133%2C%22pvid%22%3A%22454%22%2C%22sid%22%3A%2214%22%2C%22vid%22%3A%22DF0987F62DB84281BF871358C41DF2C3%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2243001159410041845018%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
            console.log(url);
            dataService.urls.push(url);
            dataService.pagefiles.push(pageFile2);
            dataService.orderids.push(orderid);
            dataService.envs.push(2 + ' ' + inputData.env2);
            //同一个订单号 算一个orderid,此处orderidindex同orderi
            dataService.orderidIndexs.push(i + 1);
        });
    }

    console.log('dataService ' + JSON.stringify(dataService));

    return dataService;
}

// var result = {}
// result.arr = [{"pvid":"9320","sid":"372","vid":"D55214F33D1E4709A7241C919B207471","type":"Hybrid","clientID":"12001127510006422203","startDate":1494817487025,"endDate":1494817727025,"extraKeys":[{"key":"oid","value":"3834430088"}]}]
// result.data = 'oid'
// var inputInfo = ["env1=fat395","env2=fat396","IMAGEPATH=D:/ImageData/postService/hybridDetail","date=20170514","datekey=20170514121945700","orderidlist=3039151353","requirePath=postService/hybridDetail/controller","selectStr=hybridOrderState","cookieLogin=88B04A49413AFCBE9D1A973C8BBE22FE57A3726A7F7EBC1E53BC1685E5C188DB"]
// var inputData = {};
//
//
// for(var item in inputInfo){
//     inputData[inputInfo[item].split('=')[0]]=inputInfo[item].split('=')[1];
// }
//
// inputData.orderid = inputData.orderidlist.indexOf(',') ? inputData.orderidlist.split(',') : inputData.orderidlist;
// //var userinfo = { UID: uid, PWD: pwd };
// //截取模块数组
// inputData.selectData = inputData.selectStr.indexOf(',') ? inputData.selectStr.split(',') : inputData.selectStr;
// //保存图片路径
// inputData.LOGPATH = inputData.IMAGEPATH + inputData.date;
// inputData.PAGEPATH = inputData.IMAGEPATH + inputData.datekey;
//
// getBaseReplayData(inputData, result);

module.exports = {
    getBaseReplayData: getBaseReplayData
};