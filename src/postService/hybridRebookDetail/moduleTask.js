
//截图任务task 按模块来写对应的函数


var utils = require('../../common/utils');
var computeSize = utils.computeSize;
var loggerInfo = utils.loggerInfo;
var loggerTime = utils.loggerTime;
var loggerDebug = utils.loggerDebug;
var computeUseTime = utils.computeUseTime;
var consoleData = {};
module.exports = {
    'getFlight':function (page, dataInfo) {
        var fightSize = page.evaluate(function () {
            window.hybridFindStart = new Date().getTime();
            var flightChange = $ && $('.flight-change');
            var orderState =  $ && $('.order-state');
            var select = [];
            if ($ && flightChange && flightChange.length
            && orderState && orderState.length) {
                //隐藏广告
                var guanggao = window.guanggao || $('#js_ads');
                window.guanggao = guanggao;
                guanggao.hide();
                //隐藏航班提示
                var hangbanLogo = window.hangbanLogo || $('.flight-section-container .airline-icon');
                window.hangbanLogo = hangbanLogo;
                hangbanLogo.hide();

                var item = flightChange;
                var position = item.offset();
                window.hybridFindEnd = new Date().getTime();
                select.push({
                    position: position,
                    // item: item.html(),

                });
                item = orderState;
                position = item.offset();
                select.push({
                    position: position,

                });
            }
            return select;
        });
        loggerDebug('fightSize', fightSize);
        if (fightSize && fightSize.length) {

            page.clipRect = computeSize(page, fightSize, 'flightChange');
            if (page.clipRect.height != 0) {
                page.render(dataInfo.PAGEPATH + 'hybridRbkFlight_success.png', {
                    format: 'png',
                    quality: '50'
                });
                page.settings.pageCount++;
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('success:  save hybridRbkFlight ',consoleData);
                // taskCallback(null, 'success, save hybridRbkFlight');

            } else {
                page.clipRect = computeSize(page, [], 'whole');
                page.render(dataInfo.PAGEPATH + 'hybridRbkFlight_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('fail:  save hybridRbkFlight ',consoleData);
                // taskCallback(null, 'fail, save hybridRbkFlight');
            }
        } else {
            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridRbkFlight_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridRbkFlight ',consoleData);
            // taskCallback(null, 'fail, save hybridRbkFlight');
        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },
    'clickFun':function(page){
        console.log('in clickFun');
        var logger = page.evaluate(function () {
            var logger = [];
            //隐藏证件上传
            var maskDiv = $ && $('.mask');
            maskDiv.hide();
            var modalRemind =  $ && $('.modal.modal-remind');
            modalRemind.hide();
            //航变通知
            var flightChange = $ && $('.popup.popup-flight-change');
            flightChange.hide();
            //隐藏广告
            var guanggao = window.guanggao || $('#js_ads');
            window.guanggao = guanggao;
            guanggao.hide();
            //用于判断是否进入了改签详情页
            var gaiqianex=$ && $('[data-hpageid="10320641845"] .hh-title').html()=='改签详情';
            //先判断改签详情是否已打开，未打开的需先打开
            if(!gaiqianex && !window.gaiqian){
                logger.push('if in click function');
                //先确认订单详情页有改签详情按钮或者航班中有改签记录入口
                var DetailButton = $ && $('.j-selfsrv-item[data-speed="改签详情-头部"]');
                var RebookState =  $ && $('.js_rebook_detail[data-seg="0"]');
                var clickEvent, clickItem;
                if ($ && DetailButton && DetailButton.length) {
                    logger.push('click rebook btn');
                    //点击订单详情页“改签详情”
                    // DetailButton.click();
                    clickEvent = document.createEvent('HTMLEvents');
                    clickEvent.initEvent('click',false,true);
                    // var clickItem = document.getElementsByClassName('button button-ghost-blue button-primary button-disabled-none j-selfsrv-item ')[0];
                    // var clickItem = document.getElementsByClassName('button button-ghost-blue button-primary j-selfsrv-item')[0];
                    clickItem = DetailButton[0];
                    clickItem.dispatchEvent(clickEvent);
                    clickItem.click();
                    window.gaiqian = true;
                }
                else if ($ && RebookState && RebookState.length) {
                    logger.push('click person');
                    clickEvent = document.createEvent('HTMLEvents');
                    clickEvent.initEvent('click',false,true);
                    clickItem = document.getElementsByClassName('flight-passanger js_rebook_detail')[0];
                    clickItem.dispatchEvent(clickEvent);
                    clickItem.click();
                    window.gaiqian= true;
                    console.log('------------------btn:111');
                }
            }

            return logger;
        });
        loggerDebug('logger click', logger);

    },
    'gethybridRbkDetail':function(page){
        console.log('in gethybridRbkDtl');
        var selectDiv = page.evaluate(function () {
            var select = {};
            var item;
            var position;
            var RebookDetail = $ && $('.change-detail');
            //获取改签详情的头部文案卡片
            var RebookDetailTitle = $ && $('[data-hpageid="10320641845"] .header-box-fixed');

            if ($ && RebookDetail && RebookDetail.length && RebookDetailTitle && RebookDetailTitle.length ) {

                //隐藏改签疑问
                var gaiqianQuestion = window.gaiqianQuestion || $('.change-section.j-faq-accordin');
                window.gaiqianQuestion = gaiqianQuestion;
                gaiqianQuestion.hide();

                item = RebookDetailTitle;
                position = item.offset();
                select.RebookDetailSize = [];
                select.RebookDetailSize.push({
                    position: position,
                    // item: item.html()
                });

                item = RebookDetail;
                position = item.offset();
                select.RebookDetailSize.push({
                    position: position,
                    // item: item.html()

                });
            }


            //获取改签详情航程和改签人
            RebookDetail = $ && $('.change-detail .flight');
            //获取改签详情的头部文案卡片
            RebookDetailTitle = $ && $('[data-hpageid="10320641845"] .header-box-fixed');

            if ($ && RebookDetail && RebookDetail.length && RebookDetailTitle && RebookDetailTitle.length ) {
                item = RebookDetailTitle;
                position = item.offset();
                select.hybridRbkDetailFpmSize = [];
                select.hybridRbkDetailFpmSize.push({
                    position: position,
                    // item: item.html()
                });
                item = RebookDetail;
                position = item.offset();

                select.hybridRbkDetailFpmSize.push({
                    position: position,
                    // item: item.html()

                });
            }

            RebookDetail= $ && $('.change-detail');
            var RebookDetailFlight= $ && $('.change-detail .flight');
            var RebookDetailPro;
            if ($('.process-axis') && $('.process-axis').length){
                RebookDetailPro= $ && $('.change-status.j-probar');
            }else
            {
                RebookDetailPro= $ && $('.j-operate');
            }

            var renderList = [RebookDetailPro,RebookDetail,RebookDetailFlight];

            select.RebookDetailProcessSize = [];
            for(var i = 0;i < renderList.length;i++) {
                if (renderList[i] && renderList[i].length) {
                    select.RebookDetailProcessSize.push({
                        position: renderList[i].offset(),
                        // item: renderList[i].html()
                    });
                }
            }


            return select;

        });
        loggerDebug('selectDiv', selectDiv);
        return selectDiv;
    },
    'hybridRbkDetail': function  (page, dataInfo, taskCallback) {

        if (dataInfo.selectDiv.RebookDetailSize && dataInfo.selectDiv.RebookDetailSize.length) {

            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.RebookDetailSize, 'add');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRbkDetail_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridRbkDetail ',consoleData);
                taskCallback(null, 'success,  save hybridRbkDetail');

            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRbkDetail_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridRbkDetail ',consoleData);
                taskCallback(null, 'fail,  save hybridRbkDetail');

            }
        } else {
            console.log('fail');
            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridRbkDetail_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridRbkDetail ',consoleData);
            taskCallback(null, 'fail,  save hybridRbkDetail');

        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);



    },
    'hybridRbkDetailFpm': function (page, dataInfo, taskCallback) {

        if (dataInfo.selectDiv.hybridRbkDetailFpmSize && dataInfo.selectDiv.hybridRbkDetailFpmSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.hybridRbkDetailFpmSize, 'add');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRbkDetailFpm_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridRbkDetailFpm ',consoleData);
                taskCallback(null, 'success,  save hybridRbkDetailFpm');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRbkDetailFpm_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridRbkDetailFpm ',consoleData);
                taskCallback(null, 'fail,  save hybridRbkDetailFpm');

            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridRbkDetailFpm_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridRbkDetailFpm ',consoleData);
            taskCallback(null, 'fail,  save hybridRbkDetailFpm');
        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);



    },

    'hybridRbkDetailProcess': function (page, dataInfo, taskCallback) {

        if (dataInfo.selectDiv.RebookDetailProcessSize && dataInfo.selectDiv.RebookDetailProcessSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.RebookDetailProcessSize, 'rbk');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRbkDetailProcess_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridRbkDetailProcess ',consoleData);
                taskCallback(null, 'success,  save hybridRbkDetailProcess');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRbkDetailProcess_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridRbkDetailProcess ',consoleData);
                taskCallback(null, 'fail,  save hybridRbkDetailProcess');
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridRbkDetailProcess_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridRbkDetailProcess ',consoleData);
            taskCallback(null, 'fail,  save hybridRbkDetailProcess');
        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);



    },
};