
//截图任务task 按模块来写对应的函数


var utils = require('../../common/utils');
var computeSize = utils.computeSize;
var loggerInfo = utils.loggerInfo;
var loggerTime = utils.loggerTime;
var loggerDebug = utils.loggerDebug;
var computeUseTime = utils.computeUseTime;
var consoleData = {};
module.exports = {
    'gethybridDetail': function(page){

        var selectDiv = page.evaluate(function () {
            window.hybridFindStart = new Date().getTime();
            //查找订单状态模块
            var orderState = $ && $('.order-state');
            var item,position;
            var select = {};
            if ($ && orderState && orderState.length) {
                //模块存在
                //查找广告模块，设置全局变量
                var guanggao =  $ && $('#js_ads');
                var orderDetailCon = $ && $('.j-orderdetail-container');
                if (orderDetailCon && orderDetailCon.length

                    && guanggao && guanggao.length) {
                    item = orderDetailCon;
                    position = item.offset();
                    //保存要返回的数据
                    select.orderDetailSize = [];
                    select.orderDetailSize.push({
                        //位置信息
                        position: position,
                        //元素信息
                        // item: item.html()
                    });
                    item = guanggao;
                    position = item.offset();
                    //保存要返回的数据
                    select.orderDetailSize.push({
                        //位置信息
                        position: position,
                        //元素信息
                        // item: item.html()
                    });
                }
                guanggao = window.guanggao || $ && $('#js_ads');
                window.guanggao = guanggao;
                //隐藏广告模块
                guanggao.hide();
                //隐藏航司log
                var hangbanLogo = window.hangbanLogo || $ && $('.flight-section-container .airline-icon');
                window.hangbanLogo = hangbanLogo;
                hangbanLogo.hide();

                //查找订单状态的位置
                item = orderState;
                position = item.offset();
                //记录运行时间，可忽略
                window.hybridFindEnd = new Date().getTime();
                //保存要返回的数据
                select.orderStateSize = [];
                select.orderStateSize.push({
                    //位置信息
                    position: position,
                    //元素信息
                    // item: item.html(),
                    //时间信息，可以忽略
                    time: window.hybridFindEnd - window.hybridFindStart
                });
                //航班信息
                var flightContainer = $('.flight-section-container');

                if ($ && flightContainer && flightContainer.length) {
                    item = flightContainer;
                    position = item.offset();
                    window.hybridFindEnd = new Date().getTime();
                    select.fightSize = [];
                    select.fightSize.push({
                        position: position,
                        // item: item.html(),
                        time: window.hybridFindEnd - window.hybridFindStart
                    });
                }


                //乘机人信息
                window.hybridFindStart = new Date().getTime();

                var passengerBox = $('.passenger-box');
                if ($
                    && passengerBox
                    && passengerBox.length) {
                    item = passengerBox;
                    position = item.offset();

                    window.hybridFindEnd = new Date().getTime();
                    select.passengerSize = [];
                    select.passengerSize.push({
                        position: position,
                        // item: item.html(),
                        time: window.hybridFindEnd - window.hybridFindStart
                    });

                }
                window.hybridFindStart = new Date().getTime();
                var xproductUse = $ && $('.form.xproduct-info.j-xprd-readybuy');

                if ($ && xproductUse && xproductUse.length) {

                    item = xproductUse;
                    position = item.offset();
                    window.hybridFindEnd = new Date().getTime();
                    select.xproductUseSize = [];
                    select.xproductUseSize.push({
                        position: position,
                        // item: item.html(),
                        time: window.hybridFindEnd - window.hybridFindStart
                    });
                }

                window.hybridFindStart = new Date().getTime();
                var jSelfSrv = $('.j-selfsrv');

                if ($ && jSelfSrv && jSelfSrv.length) {

                    item = jSelfSrv;
                    position = item.offset();
                    window.hybridFindEnd = new Date().getTime();
                    select.selfSrvSize = [];
                    select.selfSrvSize.push({
                        position: position,
                        // item: item.html(),
                        time: window.hybridFindEnd - window.hybridFindStart
                    });
                }


                window.hybridFindStart = new Date().getTime();
                var jFaq = $('.j-faq');

                if ($ && jFaq && jFaq.length) {

                    item = jFaq;
                    position = item.offset();
                    window.hybridFindEnd = new Date().getTime();
                    select.problemSectionSize = [];
                    select.problemSectionSize.push({
                        position: position,
                        // item: item.html(),
                        time: window.hybridFindEnd - window.hybridFindStart
                    });
                }
                window.hybridFindStart = new Date().getTime();
                var jSubSale = $('.j-subsale');
                if ($ && jSubSale && jSubSale.length) {

                    item = jSubSale;
                    position = item.offset();
                    window.hybridFindEnd = new Date().getTime();
                    select.subSaleSize = [];
                    select.subSaleSize.push({
                        position: position,
                        //item: item.html(),
                        time: window.hybridFindEnd - window.hybridFindStart
                    });
                }


                return select;
            }
            return select;

        });
        // loggerDebug('selectDiv', selectDiv);
        return selectDiv;
    },
    'hybridDetail': function(page, dataInfo, taskCallback){
        if (dataInfo.selectDiv.orderDetailSize && dataInfo.selectDiv.orderDetailSize.length) {
            loggerDebug('dataInfo.selectDiv.orderStateSize ', dataInfo.selectDiv.orderDetailSize );
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.orderDetailSize, 'subTwoTop');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridDetail_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridDetail ',consoleData);
                taskCallback(null, 'success, save hybridDetail');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridDetail_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridDetail ',consoleData);
                taskCallback(null, 'fail, save hybridDetail');
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridDetail_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridDetail ',consoleData);
            taskCallback(null, 'fail, save hybridDetail');

        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },
    'hybridOrderState': function (page, dataInfo, taskCallback) {
        console.log('PAGEPATH' + dataInfo.PAGEPATH);
        loggerTime('start hybridOrderState() ', page.settings.iniTime);
        //启动的phantomjs中运行js脚本
        //订单状态
        // var orderStateSize = page.evaluate(function () {
        //     window.hybridFindStart = new Date().getTime();
        //     //查找订单状态模块
        //     var orderState = $('.order-state');
        //     var select = [];
        //     if ($ && orderState && orderState.length) {
        //         //模块存在
        //         //查找广告模块，设置全局变量
        //         var guanggao = window.guanggao || $('#js_ads');
        //         window.guanggao = guanggao;
        //         //隐藏广告模块
        //         guanggao.hide();
        //         //隐藏航司log
        //         var hangbanLogo = window.hangbanLogo || $('.flight-section-container .airline-icon');
        //         window.hangbanLogo = hangbanLogo;
        //         hangbanLogo.hide();
        //         //查找订单状态的位置
        //         var item = orderState;
        //         var position = item.offset();
        //         //记录运行时间，可忽略
        //         window.hybridFindEnd = new Date().getTime();
        //         //保存要返回的数据
        //         select.push({
        //             //位置信息
        //             position: position,
        //             //元素信息
        //             item: item,
        //             //时间信息，可以忽略
        //             time: window.hybridFindEnd - window.hybridFindStart
        //         });
        //
        //     }
        //
        //
        //
        //
        //
        //     return select;
        // });
        // //打印耗时，可以忽略
        // loggerTime('findorderStateSizeDiv ', page.settings.iniTime);

        if (dataInfo.selectDiv.orderStateSize && dataInfo.selectDiv.orderStateSize.length) {
            loggerDebug('dataInfo.selectDiv.orderStateSize ', dataInfo.selectDiv.orderStateSize );
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.orderStateSize, 'normalWithoutLeft');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridOrderState_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridOrderState ',consoleData);
                taskCallback(null, 'success, save hybridOrderState');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridOrderState_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridOrderState ',consoleData);
                taskCallback(null, 'fail, save hybridOrderState');
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridOrderState_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridOrderState ',consoleData);
            taskCallback(null, 'fail, save hybridOrderState');

        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);



    },
    'hybridFlight': function (page, dataInfo, taskCallback) {
        console.log('start hybridFlight() useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');
        //航班信息
        // var fightSize = page.evaluate(function () {
        //     window.hybridFindStart = new Date().getTime();
        //     var flightContainer = $('.flight-section-container');
        //     var select = [];
        //     if ($ && flightContainer && flightContainer.length) {
        //         //隐藏广告
        //         var guanggao = window.guanggao || $('#js_ads');
        //         window.guanggao = guanggao;
        //         guanggao.hide();
        //         //隐藏航班提示
        //         var hangbanLogo = window.hangbanLogo || $('.flight-section-container .airline-icon');
        //         window.hangbanLogo = hangbanLogo;
        //         hangbanLogo.hide();
        //
        //         var item = flightContainer;
        //         var position = item.offset();
        //         window.hybridFindEnd = new Date().getTime();
        //         select.push({
        //             position: position,
        //             item: item,
        //             time: window.hybridFindEnd - window.hybridFindStart
        //         });
        //     }
        //     return select;
        // })
        // //查看耗时---无需关注
        // console.log('findUseTime: ' + fightSize[0].time);
        // console.log('findfightSizeDiv useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');

        if (dataInfo.selectDiv.fightSize && dataInfo.selectDiv.fightSize.length) {

            page.clipRect = computeSize(page, dataInfo.selectDiv.fightSize, 'normalWithoutLeft');
            if (page.clipRect.height != 0) {
                page.render(dataInfo.PAGEPATH + 'hybridFlight_success.png', {
                    format: 'png',
                    quality: '50'
                });
                page.settings.pageCount++;
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('success:  save hybridFlight ',consoleData);
                taskCallback(null, 'success, save hybridFlight');

            } else {
                page.clipRect = computeSize(page, [], 'whole');
                page.render(dataInfo.PAGEPATH + 'hybridFlight_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('fail:  save hybridFlight ',consoleData);
                taskCallback(null, 'fail, save hybridFlight');
            }
        } else {
            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridFlight_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridFlight ',consoleData);
            taskCallback(null, 'fail, save hybridFlight');
        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },
    'hybridPassenger': function (page, dataInfo, taskCallback) {
        console.log('start hybridPassenger() useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's, ' + page.settings.iniTime);
        //  var startTime = Date.now();
        // //乘机人模块
        // var passengerSize = page.evaluate(function () {
        //     window.hybridFindStart = new Date().getTime();
        //     var select = [];
        //     var passengerBox = $('.passenger-box');
        //     if ($
        //         && passengerBox
        //         && passengerBox.length) {
        //         var ifTimeStart = new Date().getTime();
        //         //隐藏广告
        //         var guanggao = window.guanggao || $('#js_ads');
        //         window.guanggao = guanggao;
        //         guanggao.hide();
        //         var guanggaoTime = new Date().getTime();
        //         var item = passengerBox;
        //         var position = item.offset();
        //         var offsetTime = new Date().getTime();
        //         window.hybridFindEnd = new Date().getTime();
        //         select.push({
        //             position: position,
        //             item: item,
        //             time: window.hybridFindEnd - window.hybridFindStart,
        //             end: window.hybridFindStart,
        //             ifTimeStart: ifTimeStart,
        //             guanggaoTime: guanggaoTime,
        //             offsetTime: offsetTime
        //         });
        //         select[0].pushTime = new Date().getTime();
        //
        //     }
        //     return select;
        // });
        //
        // //查看耗时  无需关注
        // console.log('dddd' + (Date.now() - startTime));
        // console.log('startTime' + startTime);
        // console.log('Date.now()' + Date.now());
        // console.log('pushTime:' + passengerSize[0].pushTime);
        // console.log('pushTime:' + (passengerSize[0].pushTime - startTime));
        // console.log('guanggaoTime: ' + (passengerSize[0].guanggaoTime));
        // console.log('offsetTime: ' + (passengerSize[0].offsetTime));
        // console.log('ifTime:' + (passengerSize[0].ifTimeStart));
        // console.log('evaluateUse:' + (passengerSize[0].end));
        // console.log('guanggaoTime: ' + (passengerSize[0].guanggaoTime - startTime));
        // console.log('offsetTime: ' + (passengerSize[0].offsetTime - startTime));
        // console.log('ifTime:' + (passengerSize[0].ifTimeStart - startTime));
        // console.log('evaluateUse:' + (passengerSize[0].end - startTime));
        // console.log('findUseTime: ' + passengerSize[0].time);
        // console.log('findpassengerSizeDiv useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's, ' + page.settings.iniTime);
        if (dataInfo.selectDiv.passengerSize && dataInfo.selectDiv.passengerSize.length) {

            page.clipRect = computeSize(page, dataInfo.selectDiv.passengerSize, 'normalWithoutLeft');
            if (page.clipRect.height != 0) {
                page.render(dataInfo.PAGEPATH + 'hybridPassenger_success.png', {
                    format: 'png',
                    quality: '50'
                });
                page.settings.pageCount++;
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('success:  save hybridPassenger ',consoleData);
                taskCallback(null, 'success, save hybridPassenger');

            } else {
                page.clipRect = computeSize(page, [], 'whole');
                page.render(dataInfo.PAGEPATH + 'hybridPassenger_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('fail:  save hybridPassenger ',consoleData);
                taskCallback(null, 'fail, save hybridPassenger');
            }
        } else {
            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridPassenger_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridPassenger ',consoleData);
            taskCallback(null, 'fail, save hybridPassenger');
        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);
    },
    'hybridXproductUse': function (page, dataInfo, taskCallback) {
        console.log('start hybridXproductUse() useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');
        //已购x产品 有的订单可能没有 这个怎么处理
        // var xproductUseSize = page.evaluate(function () {
        //     window.hybridFindStart = new Date().getTime();
        //     var xproductUse = $ && $('.xproduct-use');
        //     var select = [];
        //     if ($ && xproductUse && xproductUse.length) {
        //         //隐藏广告
        //         var guanggao = window.guanggao || $('#js_ads');
        //         window.guanggao = guanggao;
        //         guanggao.hide();
        //         var item = xproductUse;
        //         var position = item.offset();
        //         window.hybridFindEnd = new Date().getTime();
        //         select.push({
        //             position: position,
        //             item: item.html(),
        //             time: window.hybridFindEnd - window.hybridFindStart
        //         });
        //     }
        //     return select;
        // });
        //
        // loggerDebug('xproductUseSize',xproductUseSize);
        if (dataInfo.selectDiv.xproductUseSize && dataInfo.selectDiv.xproductUseSize.length) {

            page.clipRect = computeSize(page, dataInfo.selectDiv.xproductUseSize, 'normalWithoutLeft');
            if (page.clipRect.height > 1) {
                page.settings.pageCount++;
                page.render(dataInfo.PAGEPATH + 'hybridXproductUse_success.png', {
                    format: 'png',
                    quality: '50'
                });
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('success:  save hybridXproductUse ',consoleData);
                taskCallback(null, 'success, save hybridXproductUse');
            } else {
                //高度为0时，再判断
                var notLoading = page.evaluate(function () {
                    //都不存在，则返回true
                    return !!(!($('.form.passenger.section').length || $('.order-price.js_price_detail').length || $('.flight-section-container').length) || $('.ui-loading')[0].style.display != 'none' || $('.error-box-btn').length);
                });
                if (notLoading) {
                    //都未加载，算失败
                    page.clipRect = computeSize(page, [], 'whole');
                    page.render(dataInfo.PAGEPATH + 'hybridXproductUse_fail.png', {
                        format: 'png',
                        quality: '50'
                    });
                    consoleData = {
                        env: dataInfo.env,
                        orderid: dataInfo.orderid,
                        number: dataInfo.orderidIndex,
                    };
                    loggerInfo('fail:  save hybridXproductUse ',consoleData);
                    taskCallback(null, 'fail, save hybridXproductUse');
                } else {
                    page.settings.pageCount++;
                    page.clipRect = computeSize(page, [], 'whole');
                    page.render(dataInfo.PAGEPATH + 'hybridXproductUse_withoutXproduct.png', {
                        format: 'png',
                        quality: '50'
                    });
                    consoleData = {
                        env: dataInfo.env,
                        orderid: dataInfo.orderid,
                        number: dataInfo.orderidIndex,
                    };
                    loggerInfo('success:  save hybridXproductUse ',consoleData);
                    taskCallback(null, 'success, save hybridXproductUse');
                }
                
            }          
        } else {
            page.clipRect = computeSize(page, [], 'whole');
            //console.log('flight fail');
            page.render(dataInfo.PAGEPATH + 'hybridXproductUse_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridXproductUse ',consoleData);
            taskCallback(null, 'fail, save hybridXproductUse');
        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);
    },
    'hybridSelfSrv': function (page, dataInfo, taskCallback) {
        console.log('start hybridSelfSrv() useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');
        //  //自助服务模块
        // var selfSrvSize = page.evaluate(function () {
        //     window.hybridFindStart = new Date().getTime();
        //     var jSelfSrv = $('.j-selfsrv');
        //     var select = [];
        //     if ($ && jSelfSrv && jSelfSrv.length) {
        //         //隐藏广告
        //         var guanggao = window.guanggao || $('#js_ads');
        //         window.guanggao = guanggao;
        //         guanggao.hide();
        //         var item = jSelfSrv;
        //         var position = item.offset();
        //         window.hybridFindEnd = new Date().getTime();
        //         select.push({
        //             position: position,
        //             item: item,
        //             time: window.hybridFindEnd - window.hybridFindStart
        //         });
        //     }
        //     return select;
        // })
        // console.log('findUseTime: ' + selfSrvSize[0].time);
        // console.log('findselfSrvSizeDiv useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');
        if (dataInfo.selectDiv.selfSrvSize && dataInfo.selectDiv.selfSrvSize.length) {

            page.clipRect = computeSize(page, dataInfo.selectDiv.selfSrvSize, 'normalWithoutLeft');
            if (page.clipRect.height > 1) {
                page.settings.pageCount++;
                page.render(dataInfo.PAGEPATH + 'hybridSelfSrv_success.png', {
                    format: 'png',
                    quality: '50'
                });
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('success:  save hybridSelfSrv ',consoleData);
                taskCallback(null, 'success, save hybridSelfSrv');
            } else {
                //高度为0时，再判断
                var notLoading = page.evaluate(function () {
                    //都不存在，则返回true
                    return !!(!($('.form.passenger.section').length || $('.order-price.js_price_detail').length || $('.flight-section-container').length) || $('.ui-loading')[0].style.display != 'none' || $('.error-box-btn').length);
                });
                if (notLoading) {
                    //都未加载，算失败
                    page.clipRect = computeSize(page, [], 'whole');
                    page.render(dataInfo.PAGEPATH + 'hybridSelfSrv_fail.png', {
                        format: 'png',
                        quality: '50'
                    });
                    consoleData = {
                        env: dataInfo.env,
                        orderid: dataInfo.orderid,
                        number: dataInfo.orderidIndex,
                    };
                    loggerInfo('fail:  save hybridSelfSrv ',consoleData);
                    taskCallback(null, 'fail, save hybridSelfSrv');
                } else {

                    page.settings.pageCount++;
                    page.clipRect = computeSize(page, [], 'whole');
                    page.render(dataInfo.PAGEPATH + 'hybridSelfSrv_withoutXproduct.png', {
                        format: 'png',
                        quality: '50'
                    });
                    consoleData = {
                        env: dataInfo.env,
                        orderid: dataInfo.orderid,
                        number: dataInfo.orderidIndex,
                    };
                    loggerInfo('success:  save hybridSelfSrv ',consoleData);
                    taskCallback(null, 'success, save hybridSelfSrv');


                    // page.settings.pageCount++;
                    // page.clipRect = computeSize(page, [], 'whole');
                    // page.render(dataInfo.PAGEPATH + 'hybridSelfSrv_fail.png', {
                    //     format: 'png',
                    //     quality: '50'
                    // });
                    // consoleData = {
                    //     env: dataInfo.env,
                    //     orderid: dataInfo.orderid,
                    //     number: dataInfo.orderidIndex,
                    // }
                    // loggerInfo('fail:  save hybridSelfSrv ',consoleData);
                    // taskCallback(null, 'fail, save hybridSelfSrv');
                }

               
            }
            
        } else {
            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridSelfSrv_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridSelfSrv ',consoleData);
            taskCallback(null, 'fail, save hybridSelfSrv');

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);
    },
    'hybridProblemSection': function (page, dataInfo, taskCallback) {
        console.log('start hybridProblemSection() useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');
        //订单可能遇到的问题
        // var problemSectionSize = page.evaluate(function () {
        //     window.hybridFindStart = new Date().getTime();
        //     var jFaq = $('.j-faq');
        //     var select = [];
        //     if ($ && jFaq && jFaq.length) {
        //         //隐藏广告
        //         var guanggao = window.guanggao || $('#js_ads');
        //         window.guanggao = guanggao;
        //         guanggao.hide();
        //         var item = jFaq;
        //         var position = item.offset();
        //         window.hybridFindEnd = new Date().getTime();
        //         select.push({
        //             position: position,
        //             item: item,
        //             time: window.hybridFindEnd - window.hybridFindStart
        //         });
        //     }
        //     return select;
        // })
        // console.log('findUseTime: ' + problemSectionSize[0].time);
        // console.log('findproblemSectionSizeDiv useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');
        if (dataInfo.selectDiv.problemSectionSize && dataInfo.selectDiv.problemSectionSize.length) {

            page.clipRect = computeSize(page, dataInfo.selectDiv.problemSectionSize, 'normalWithoutLeft');
            if (page.clipRect.height != 0) {
                page.render(dataInfo.PAGEPATH + 'hybridProblemSection_success.png', {
                    format: 'png',
                    quality: '50'
                });
                page.settings.pageCount++;
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('success:  save hybridProblemSection ',consoleData);
                taskCallback(null, 'success, save hybridProblemSection');

            } else {
                page.clipRect = computeSize(page, [], 'whole');
                page.render(dataInfo.PAGEPATH + 'hybridProblemSection_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('fail:  save hybridProblemSection ',consoleData);
                taskCallback(null, 'fail, save hybridProblemSection');
            }
        } else {
            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridProblemSection_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridProblemSection ',consoleData);
            taskCallback(null, 'fail, save hybridProblemSection');

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);
    },
    'hybridSubSale': function (page, dataInfo, taskCallback) {
        console.log('start hybridSubSale() useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');
        //次级搭售
        // var subSaleSize = page.evaluate(function () {
        //     window.hybridFindStart = new Date().getTime();
        //     var select = [];
        //     var jSubSale = $('.j-subsale');
        //     if ($ && jSubSale && jSubSale.length) {
        //         //隐藏广告
        //         var guanggao = window.guanggao || $('#js_ads');
        //         window.guanggao = guanggao;
        //         guanggao.hide();
        //         var item = jSubSale;
        //         var position = item.offset();
        //         window.hybridFindEnd = new Date().getTime();
        //         select.push({
        //             position: position,
        //             item: item,
        //             time: window.hybridFindEnd - window.hybridFindStart
        //         });
        //     }
        //     return select;
        // })
        // console.log('findUseTime: ' + subSaleSize[0].time);
        // console.log('findsubSaleSizeDiv useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');
        if (dataInfo.selectDiv.subSaleSize && dataInfo.selectDiv.subSaleSize.length) {

            page.clipRect = computeSize(page, dataInfo.selectDiv.subSaleSize, 'normalWithoutLeft');
            if (page.clipRect.height != 0) {
                page.render(dataInfo.PAGEPATH + 'hybridSubSale_success.png', {
                    format: 'png',
                    quality: '50'
                });
                page.settings.pageCount++;
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('success:  save hybridSubSale ',consoleData);
                taskCallback(null, 'success, save hybridSubSale');
            } else {
                page.clipRect = computeSize(page, [], 'whole');
                page.render(dataInfo.PAGEPATH + 'hybridSubSale_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('fail:  save hybridSubSale ',consoleData);
                taskCallback(null, 'fail, save hybridSubSale');
            }
        } else {
            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridSubSale_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridSubSale ',consoleData);
            taskCallback(null, 'fail, save hybridSubSale');

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);
    }
};