//计算页面截图所需的基础数据信息

//截图可选环境url
var url395 = 'http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid={Orderid}';
var url396 = 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid={Orderid}';

//截图所需基础数据，必传
var dataService = {
    //截图所需url
    urls: [],
    //日志展示用的环境
    envs: [],
    //每个page存放路径
    pagefiles: [],
    //截图的订单号
    orderids: [],
    //截图的第几个订单号
    orderidIndexs: [],
    //log保存路径
    LOGPATH: '',
    //截图page保存路径
    PAGEPATH:'',
    //保存整图的时候，页面名称
    wholePageName: 'hybridDetail',
};

function getBaseData(inputData) {
    var pageFile1 = '';
    var pageFile2 = '';

    var urlHybridDetail1 = '';
    var urlHybridDetail2 = '';
    //设置log及图片保存位置  无需修改
    if(dataService  && dataService.LOGPATH ===''){
        dataService.LOGPATH = inputData.LOGPATH;
    }
    if(dataService &&  dataService.PAGEPATH ===''){
        dataService.PAGEPATH = inputData.PAGEPATH;
    }
    //根据可选环境，生成截图所需访问的urls,
    if (inputData.env1 == 'fat395') {
        urlHybridDetail1 = url395;
        pageFile1 = dataService.PAGEPATH + '-env1-' + inputData.env1 + '/';
    } else if (inputData.env1 == 'fat396') {
        urlHybridDetail1 = url396;
        pageFile1 = dataService.PAGEPATH + '-env1-' + inputData.env1 + '/';
    }
    if (inputData.env2 == 'fat395') {
        urlHybridDetail2 = url395;
        pageFile2 = dataService.PAGEPATH + '-env2-' + inputData.env2 + '/';
    } else if (inputData.env2 == 'fat396') {
        urlHybridDetail2 = url396;
        pageFile2 = dataService.PAGEPATH + '-env2-' + inputData.env2 + '/';
    }
    if(inputData.orderid && inputData.orderid.length){
        for (var i = 0; i < inputData.orderid.length; i++) {
            var url = urlHybridDetail1.replace(/\{Orderid\}/, inputData.orderid[i]);
            // url = 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3787105441&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494839709794%2C%22endDate%22%3A1494839949794%2C%22pvid%22%3A%22945%22%2C%22sid%22%3A%2250%22%2C%22vid%22%3A%22B71C6E5C9E254EE5B848CCBD44C0E08B%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2212001175210025564216%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
            // url ='http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3940116072&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1495123073133%2C%22endDate%22%3A1495123313133%2C%22pvid%22%3A%22454%22%2C%22sid%22%3A%2214%22%2C%22vid%22%3A%22DF0987F62DB84281BF871358C41DF2C3%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2243001159410041845018%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
            dataService.urls.push(url);
            dataService.pagefiles.push(pageFile1);
            dataService.orderids.push(inputData.orderid[i]);
            dataService.envs.push(1 + ' ' + inputData.env1);
            //同一个订单号 算一个orderid,此处orderidindex同orderid保持一致，orderid相同的orderidIndex相同
            dataService.orderidIndexs.push(i + 1);
            url = urlHybridDetail2.replace(/\{Orderid\}/, inputData.orderid[i]);
            // url = 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3787105441&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1494839709794%2C%22endDate%22%3A1494839949794%2C%22pvid%22%3A%22945%22%2C%22sid%22%3A%2250%22%2C%22vid%22%3A%22B71C6E5C9E254EE5B848CCBD44C0E08B%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2212001175210025564216%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
            // url = 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=3940116072&replay=%7B%22listMap%22%3A%7B%22%2Fwebapp%2Fflight%2Forderdetail%2Forderdetail.html%22%3A%7B%22startDate%22%3A1495123073133%2C%22endDate%22%3A1495123313133%2C%22pvid%22%3A%22454%22%2C%22sid%22%3A%2214%22%2C%22vid%22%3A%22DF0987F62DB84281BF871358C41DF2C3%22%7D%7D%2C%22proxyUrl%22%3A%22http%3A%2F%2F10.18.6.135%3A37016%2Fproxies%2F%22%2C%22clientID%22%3A%2243001159410041845018%22%2C%22isFat%22%3Afalse%2C%22type%22%3A%22Hybrid%22%7D'
            dataService.urls.push(url);
            dataService.pagefiles.push(pageFile2);
            dataService.orderids.push(inputData.orderid[i]);
            dataService.envs.push(2 + ' ' + inputData.env2);
            //同一个订单号 算一个orderid,此处orderidindex同orderid保持一致，orderid相同的orderidIndex相同
            dataService.orderidIndexs.push(i + 1);
        }
    }

    console.log('dataService ' + JSON.stringify(dataService));

    return dataService;
}

module.exports = {
    getBaseData: getBaseData
};