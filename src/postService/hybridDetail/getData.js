/**
 * Created by lsyang on 2017-05-10 0010.
 */
var fetch = require('node-fetch');
var url = 'http://10.2.72.113/ESLogProcessor/api/ESLog/SearchEsLog';
// url = 'http://10.32.184.160/UtitilyWS/ESLogProcessor/api/ESLog/SearchEsLog';
var requestData = {
    pageid: '10320608519',
    count: 10,
    ubtKeys: [
        {
            key: 'custom_key',
            value: '_flight_hybird_ctrip'
        }
    ],
    dataKeys: [{
        key:'oid',
        path:['order','oid']
    }]
};
var data = 'oid';
// requestData = 'test'
function getDataRes(requestData, data){

    console.log(JSON.stringify(requestData));
    // post with JSON

    return fetch(url, {
        method: 'POST',
        body:    JSON.stringify(requestData),
        headers: { 'Content-Type': 'application/json' },
    })
        .then(res => res.json())
        .then(json => {
            console.log(json);
            if(json && json.resultData && json.resultData.length){
                console.log('调用接口成功：'+ json.resultData);

            }else{
                console.log('调用接口失败：'+ json.resultMsg);
            }
            var result = {
                requestData: requestData,
                resultData: json.resultData,
                data: data
            };

            return result;
        });
}
//
// var urlData = {
//     "listMap": {
//         "/webapp/flight/orderdetail/orderdetail.html": {
//             "startDate": 1492531200000,
//             "endDate": 1492617599999,
//             "pvid": "",
//             "sid": "",
//             "vid": ""
//         }
//     },
//     "proxyUrl": "http://10.2.74.93:37016/proxies/",
//     "clientID": "",
//     "isFat": true,
//     "type": "H5"
// }

function getUrl(requestData, data){
    return getDataRes(requestData, data).then((res) => {
        

        return res.resultData.map(item => {
            const baseData = {

                'listMap': {
                    '/webapp/flight/orderdetail/orderdetail.html': {
                        'startDate': item.startDate,
                        'endDate': item.endDate,
                        'pvid': item.pvid,
                        'sid': item.sid,
                        'vid': item.vid
                    }
                },
                'proxyUrl': 'http://10.18.6.135:37016/proxies/',
                'clientID': item.clientID,
                'isFat': false,
                'type': item.type
            };
            var value = item.extraKeys.find(item => item.key == res.data).value;
            const replay = encodeURIComponent(JSON.stringify(baseData));
            const baseUrl = [];
            // baseUrl.push(`http://m.ctripcorprip.fat395.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=${value}&replay=${replay}`);
            baseUrl.push(`http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid=${value}&replay=${replay}`);

            return baseUrl;
        });
    }).then(urls => {
        // console.log(urls.concat.apply( [], urls));
        return urls.concat.apply( [], urls);
    });

}

// getUrl(requestData, data);
// getDataReq(requestData);

module.exports = {
    getUrl: getUrl,
    getDataRes: getDataRes
};