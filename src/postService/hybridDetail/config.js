
//截取模块依赖的接口 需监控的css、js

//需要截取的模块（div），依赖的接口
//公共模块
var commonMod = [
    {
        //依赖的url
        url: 'OrderDetailSearchV2',
        //url对应的正则
        urlRe: /OrderDetailSearchV2/,
        //是否发起请求，暂时未用该数据
        isReqSucc: false,
        //是否接收到返回 默认false
        isResSucc: false
    },
    /*
    {
        url: 'orderdetail-flight.js?',
        urlRe: /orderdetail-flight\.js\?/,
        isReqSucc: false,
        isResSucc: false
    },*/
    /*
    {
        url: 'fio/Notice/query?',
        urlRe: /fio\/Notice\/query\?/,
        isReqSucc: false,
        isResSucc: false
    },*/
    /*{
         url: 'detail-base.min.css?',
         urlRe: /detail-base\.min\.css\?/,
         method: 'GET',
         resType: 'text/css',
         isReqSucc: false,
         isResSucc: false
     },
     {
         url: 'detail.min.css?',
         urlRe: /detail\.min\.css\?/,
         method: 'GET',
         resType: 'text/css',
         isReqSucc: false,
         isResSucc: false
     }*/
];

//各个模块对应所需判断的接口
var ajaxHash = {
    //订单状态模块
    'hybridOrderState': [{
        url: 'OrderDetailSearchV2',
        urlRe: /OrderDetailSearchV2/,
        isReqSucc: false,
        isResSucc: false
    },
    ],
    'hybridFlight': [{
        url: 'OrderDetailSearchV2',
        urlRe: /OrderDetailSearchV2/,
        isReqSucc: false,
        isResSucc: false
    },
       
    ],
    'hybridPassenger': [{
        url: 'OrderDetailSearchV2',
        urlRe: /OrderDetailSearchV2/,
        isReqSucc: false,
        isResSucc: false
    },

    ],
    'hybridXproductUse': [{
        url: 'OrderDetailSearchV2',
        urlRe: /OrderDetailSearchV2/,
        isReqSucc: false,
        isResSucc: false
    },],
    'hybridSelfSrv': [{
        url: 'OrderDetailSearchV2',
        urlRe: /OrderDetailSearchV2/,
        isReqSucc: false,
        isResSucc: false
    },
    ],
    'hybridProblemSection': [{
        url: 'OrderDetailSearchV2',
        urlRe: /OrderDetailSearchV2/,
        isReqSucc: false,
        isResSucc: false
    },

    ],
    'hybridSubSale': [{
        url: 'OrderDetailSearchV2',
        urlRe: /OrderDetailSearchV2/,
        isReqSucc: false,
        isResSucc: false
    },

        /*
         {
         url: 'fio/Notice/query?',
         urlRe: /fio\/Notice\/query\?/,
         isReqSucc: false,
         isResSucc: false
         }*/],
};

//需要监控的css
var cssData = [
    /detail(\.min)?\.css/,
    /detail-base(\.min)?\.css/
];
//需要监控的js
var jsData = [
    /common\.js/,
    /orderdetail-flight\.js/,
    /lizard\.lite\.min\.js/
];
//添加公共模块  将公共模块和个模块需要的数据合并
for (var item in ajaxHash) {
    ajaxHash[item] = ajaxHash[item].concat(commonMod);
}

module.exports = {
    ajaxHash: ajaxHash,
    cssData: cssData,
    jsData: jsData
};