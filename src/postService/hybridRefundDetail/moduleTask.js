
//截图任务task 按模块来写对应的函数


var utils = require('../../common/utils');
var computeSize = utils.computeSize;
var loggerInfo = utils.loggerInfo;
var loggerTime = utils.loggerTime;
var loggerDebug = utils.loggerDebug;
var computeUseTime = utils.computeUseTime;
var consoleData = {};
module.exports = {
    'getFlight':function (page, dataInfo) {
        var fightSize = page.evaluate(function () {
            window.hybridFindStart = new Date().getTime();
            var flightChange = $ && $('.flight-change');
            var orderState =  $ && $('.order-state');
            var select = [];
            if ($ && flightChange && flightChange.length
                && orderState && orderState.length) {
                //隐藏广告
                var guanggao = window.guanggao || $('#js_ads');
                window.guanggao = guanggao;
                guanggao.hide();
                //隐藏航班提示
                var hangbanLogo = window.hangbanLogo || $('.flight-section-container .airline-icon');
                window.hangbanLogo = hangbanLogo;
                hangbanLogo.hide();

                var item = flightChange;
                var position = item.offset();
                window.hybridFindEnd = new Date().getTime();
                select.push({
                    position: position,
                    // item: item.html(),

                });
                item = orderState;
                position = item.offset();
                select.push({
                    position: position,

                });
            }
            return select;
        });
        loggerDebug('fightSize', fightSize);
        if (fightSize && fightSize.length) {

            page.clipRect = computeSize(page, fightSize, 'flightChange');
            if (page.clipRect.height != 0) {
                page.render(dataInfo.PAGEPATH + 'hybridRfdFlight_success.png', {
                    format: 'png',
                    quality: '50'
                });
                page.settings.pageCount++;
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('success:  save hybridRfdFlight ',consoleData);
                // taskCallback(null, 'success, save hybridRfdFlight');

            } else {
                page.clipRect = computeSize(page, [], 'whole');
                page.render(dataInfo.PAGEPATH + 'hybridRfdFlight_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('fail:  save hybridRfdFlight ',consoleData);
                // taskCallback(null, 'fail, save hybridRfdFlight');
            }
        } else {
            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridRfdFlight_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridRfdFlight ',consoleData);
            // taskCallback(null, 'fail, save hybridRfdFlight');
        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },
    'clickFun': function(page){
        console.log('in click');
        var logger = page.evaluate(function () {
            var logger = [];
            //隐藏证件上传
            var maskDiv = $ && $('.mask');
            maskDiv.hide();
            var modalRemind =  $ && $('.modal.modal-remind');
            modalRemind.hide();
            //航变通知
            var flightChange = $ && $('.popup.popup-flight-change');
            flightChange.hide();
            //隐藏订单详情页广告
            var guanggao = window.guanggao || $('#js_ads');
            window.guanggao = guanggao;
            guanggao.hide();
            //用于判断是否进入了退票详情页
            var tuipiaoex=$ && $('[data-hpageid="10320641843"] .hh-title').html()=='退票详情';
            //先判断退票详情是否已打开，未打开的需先打开
            if(!tuipiaoex && !window.tuipiao){
                logger.push('if in click function');
                //先确认订单详情页有退票详情按钮或者航班中有退票记录入口
                var DetailButton = $ && $('.j-selfsrv-item[data-speed="退票详情-头部"]');
                var RefundState =  $ && $('.js_refund_detail[data-seg="0"]');
                var clickEvent, clickItem;
                if ($ && DetailButton && DetailButton.length) {
                    logger.push('refund btn');
                    //点击订单详情页“退票详情”
                    // DetailButton.click();
                    clickEvent = document.createEvent('HTMLEvents');
                    clickEvent.initEvent('click',false,true);
                    // var clickItem = document.getElementsByClassName('button button-ghost-blue button-primary j-selfsrv-item')[1];
                    clickItem = DetailButton[0];
                    clickItem.dispatchEvent(clickEvent);

                    clickItem.click();
                    window.tuipiao = true;
                }
                else if ($ && RefundState && RefundState.length) {
                    logger.push('person');
                    clickEvent = document.createEvent('HTMLEvents');
                    clickEvent.initEvent('click',false,true);
                    clickItem = document.getElementsByClassName('flight-passanger js_refund_detail')[0];
                    clickItem.dispatchEvent(clickEvent);
                    clickItem.click();
                    window.tuipiao = true;
                }
            }
            return logger;
        });
        loggerDebug('logger click', logger);
    },
    'gethybridRfdDetail': function(page){
        var selectDiv = page.evaluate(function () {
            var select = {};
            var item;
            var position;
            select.a = [];
            select.a.push(1);
            //获取除开头部文案的卡片
            var RefundDetail = $ && $('.refund-detail.j-refund-container');
            //获取头部文案的卡片
            var RefundDetailTitle = $ && $('[data-hpageid="10320641843"] .j-header.header-box');

            if ($ && RefundDetail && RefundDetail.length && RefundDetailTitle && RefundDetailTitle.length) {

                //隐藏退票疑问
                var tuipiaoQuestion = window.tuipiaoQuestion || $('.refund-section.j-faq-accordin');
                window.tuipiaoQuestion = tuipiaoQuestion;
                tuipiaoQuestion.hide();

                item = RefundDetailTitle;
                position = item.offset();
                select.RefundDetailSize = [];
                select.RefundDetailSize.push({
                    position: position,
                    // item: item.html()
                });
                item = RefundDetail;
                position = item.offset();
                window.hybridFindEnd = new Date().getTime();
                select.RefundDetailSize.push({
                    position: position,
                    // item: item.html()
                });
            }


            RefundDetail = $ && $('.refund-detail.j-refund-container>.refund-section');
            //获取头部文案的卡片
            RefundDetailTitle = $ && $('[data-hpageid="10320641843"] .j-header.header-box');
            if ($ && RefundDetail && RefundDetail.length && RefundDetailTitle && RefundDetailTitle.length) {

                item = RefundDetailTitle;
                position = item.offset();
                select.hybridRfdDetailFpmSize = [];
                select.hybridRfdDetailFpmSize.push({
                    position: position,
                    // item: item.html()
                });
                item = RefundDetail;
                position = item.offset();
                window.hybridFindEnd = new Date().getTime();
                select.hybridRfdDetailFpmSize.push({
                    position: position,
                    // item: item.html()

                });

            }


            RefundDetail = $ && $('.allContons');
            if ($ && RefundDetail && RefundDetail.length ) {
                item = RefundDetail;
                position = item.offset();
                window.hybridFindEnd = new Date().getTime();
                select.hybridRfdDetailProcessSize = [];
                select.hybridRfdDetailProcessSize.push({
                    position: position,
                    // item: item.html()
                });
            }

            return select;

        });
        loggerDebug('selectDiv', selectDiv);
        return selectDiv;
    },
    //整屏截图，退票疑问隐藏
    'hybridRfdDetail': function  (page, dataInfo, taskCallback) {
        console.log('PAGEPATH' + dataInfo.PAGEPATH);
        loggerTime('start hybridRfdDetail() ', page.settings.iniTime);
        //启动的phantomjs中运行js脚本
        // var logger = page.evaluate(function () {
        //     var logger = [];
        //     //隐藏订单详情页广告
        //     var guanggao = window.guanggao || $('#js_ads');
        //     window.guanggao = guanggao;
        //     guanggao.hide();
        //     //用于判断是否进入了退票详情页
        //     var tuipiaoex=$ && $('[data-hpageid="10320641843"] .hh-title').html()=='退票详情';
        //     //先判断退票详情是否已打开，未打开的需先打开
        //     if(!tuipiaoex && !window.tuipiao){
        //         logger.push('if in click function');
        //         //先确认订单详情页有退票详情按钮或者航班中有退票记录入口
        //         var DetailButton = $ && $('.j-selfsrv-item[data-speed="退票详情-头部"]');
        //         var RefundState =  $ && $('.js_refund_detail[data-seg="0"]');
        //         if ($ && DetailButton && DetailButton.length) {
        //             logger.push('refund btn');
        //             //点击订单详情页“退票详情”
        //             // DetailButton.click();
        //             var clickEvent = document.createEvent('HTMLEvents');
        //             clickEvent.initEvent('click',false,true);
        //             var clickItem = document.getElementsByClassName('button button-ghost-blue button-primary j-selfsrv-item')[1];
        //             clickItem.dispatchEvent(clickEvent);
        //             clickItem.click();
        //             window.tuipiao = true;
        //         }
        //         else if ($ && RefundState && RefundState.length) {
        //             logger.push('person');
        //             var clickEvent = document.createEvent('HTMLEvents');
        //             clickEvent.initEvent('click',false,true);
        //             var clickItem = document.getElementsByClassName('flight-passanger js_refund_detail')[0];
        //             clickItem.dispatchEvent(clickEvent);
        //             clickItem.click();
        //             window.tuipiao = true;
        //         }
        //     }
        //     return logger;
        // });
        // loggerDebug('logger click', logger);

        //强制等待5秒后执行退票详情获取

        // consoleData = {
        //     env: dataInfo.env,
        //     orderid: dataInfo.orderid,
        //     number: dataInfo.orderidIndex,
        // }
        // console.log('***********************************************************ddddd' + JSON.stringify(consoleData));
        // var RefundDetailSize = page.evaluate(function () {
        //     var select = [];
        //     //获取除开头部文案的卡片
        //     var RefundDetail = $ && $('.refund-detail.j-refund-container');
        //     //获取头部文案的卡片
        //     var RefundDetailTitle = $ && $('[data-hpageid="10320641843"] .j-header.header-box');
        //     var item;
        //     var position;
        //     if ($ && RefundDetail && RefundDetail.length && RefundDetailTitle && RefundDetailTitle.length) {
        //
        //         //隐藏退票疑问
        //         var tuipiaoQuestion = window.tuipiaoQuestion || $('.refund-section.j-faq-accordin');
        //         window.tuipiaoQuestion = tuipiaoQuestion;
        //         tuipiaoQuestion.hide();
        //
        //         item = RefundDetailTitle;
        //         position = item.offset();
        //         select.push({
        //             position: position,
        //             item: item
        //         });
        //         item = RefundDetail;
        //         position = item.offset();
        //         window.hybridFindEnd = new Date().getTime();
        //         select.push({
        //             position: position,
        //             item: item
        //
        //         });
        //
        //
        //     }else{
        //         select.push('11111');
        //     }
        //     return select;
        // });
        // // loggerDebug('******************', RefundDetailSize);
        // console.log('RefundDetailSize '+ RefundDetailSize);
        //打印耗时，可以忽略
        // loggerTime('RefundDetailSizeDiv ', page.settings.iniTime);
        if (dataInfo.selectDiv.RefundDetailSize && dataInfo.selectDiv.RefundDetailSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.RefundDetailSize, 'add');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRfdDetail_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridRfdDetail ',consoleData);
                taskCallback(null, 'success,  save hybridRfdDetail');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRfdDetail_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridRfdDetail ',consoleData);
                taskCallback(null, 'fail,  save hybridRfdDetail');
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridRfdDetail_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridRfdDetail ',consoleData);
            taskCallback(null, 'fail,  save hybridRfdDetail');
        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);



    },
    //只截取头部和退票行程乘机人卡片
    'hybridRfdDetailFpm': function  (page, dataInfo, taskCallback) {
        console.log('PAGEPATH' + dataInfo.PAGEPATH);
        loggerTime('start hybridRfdDetailFpm() ', page.settings.iniTime);
        //启动的phantomjs中运行js脚本
        // var logger = page.evaluate(function () {
        //     var logger = [];
        //     //隐藏订单详情页广告
        //     var guanggao = window.guanggao || $('#js_ads');
        //     window.guanggao = guanggao;
        //     guanggao.hide();
        //     //用于判断是否进入了退票详情页
        //     var tuipiaoex=$ && $('[data-hpageid="10320641843"] .hh-title').html()=='退票详情';
        //     //先判断退票详情是否已打开，未打开的需先打开
        //     if(!tuipiaoex && !window.tuipiao){
        //         logger.push('if in click function');
        //         //先确认订单详情页有退票详情按钮或者航班中有退票记录入口
        //         var DetailButton = $ && $('.j-selfsrv-item[data-speed="退票详情-头部"]');
        //         var RefundState =  $ && $('.js_refund_detail[data-seg="0"]');
        //         if ($ && DetailButton && DetailButton.length) {
        //             logger.push('refund btn');
        //             //点击订单详情页“退票详情”
        //             // DetailButton.click();
        //             var clickEvent = document.createEvent('HTMLEvents');
        //             clickEvent.initEvent('click',false,true);
        //             var clickItem = document.getElementsByClassName('button button-ghost-blue button-primary j-selfsrv-item')[1];
        //             clickItem.dispatchEvent(clickEvent);
        //             clickItem.click();
        //             window.tuipiao = true;
        //         }
        //         else if ($ && RefundState && RefundState.length) {
        //             logger.push('person');
        //             var clickEvent = document.createEvent('HTMLEvents');
        //             clickEvent.initEvent('click',false,true);
        //             var clickItem = document.getElementsByClassName('flight-passanger js_refund_detail')[0];
        //             clickItem.dispatchEvent(clickEvent);
        //             clickItem.click();
        //             window.tuipiao = true;
        //         }
        //     }
        //     return logger;
        // });
        // loggerDebug('logger click', logger);

        //强制等待5秒后执行退票详情获取

        // var hybridRfdDetailFpmSize = page.evaluate(function () {
        //     var select = [];
        //     //获取除开头部文案的卡片
        //     var RefundDetail = $ && $('.refund-detail.j-refund-container>.refund-section');
        //
        //     //获取头部文案的卡片
        //     var RefundDetailTitle = $ && $('[data-hpageid="10320641843"] .j-header.header-box');
        //     var item;
        //     var position;
        //     if ($ && RefundDetail && RefundDetail.length && RefundDetailTitle && RefundDetailTitle.length) {
        //
        //         item = RefundDetailTitle;
        //         position = item.offset();
        //         select.push({
        //             position: position,
        //             item: item
        //         });
        //         item = RefundDetail;
        //         position = item.offset();
        //         window.hybridFindEnd = new Date().getTime();
        //         select.push({
        //             position: position,
        //             item: item.html()
        //
        //         });
        //
        //
        //
        //     }else{
        //         select.push('11111');
        //     }
        //     return select;
        // });
        //
        // //打印耗时，可以忽略
        // loggerTime('RefundDetailSizeDiv ', page.settings.iniTime);
        // console.log('RefundDetailSizeDiv '+ hybridRfdDetailFpmSize);

        if (dataInfo.selectDiv.hybridRfdDetailFpmSize && dataInfo.selectDiv.hybridRfdDetailFpmSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.hybridRfdDetailFpmSize, 'add');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRfdDetailFpm_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridRfdDetailFpm',consoleData);
                taskCallback(null, 'success,  save hybridRfdDetailFpm');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRfdDetailFpm_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridRfdDetailFpm ',consoleData);
                taskCallback(null, 'fail,  save hybridRfdDetailFpm');
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridRfdDetailFpm_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridRfdDetailFpm',consoleData);
            taskCallback(null, 'fail,  save hybridRfdDetailFpm');
        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);



    },
    //只截取退票处理,历史退票单和继续退票申请按钮
    'hybridRfdDetailProcess': function (page, dataInfo, taskCallback) {
        console.log('PAGEPATH' + dataInfo.PAGEPATH);
        loggerTime('start hybridRfdDetailProcess() ', page.settings.iniTime);
        //启动的phantomjs中运行js脚本
        // var logger = page.evaluate(function () {
        //     var logger = [];
        //     //隐藏订单详情页广告
        //     var guanggao = window.guanggao || $('#js_ads');
        //     window.guanggao = guanggao;
        //     guanggao.hide();
        //
        //     //用于判断是否进入了退票详情页
        //     var tuipiaoex=$ && $('[data-hpageid="10320641843"] .hh-title').html()=='退票详情';
        //     //先判断退票详情是否已打开，未打开的需先打开
        //     if(!tuipiaoex && !window.tuipiao){
        //         //先确认订单详情页有退票详情按钮或者航班中有退票记录入口
        //         var DetailButton = $ && $('.j-selfsrv-item[data-speed="退票详情-头部"]');
        //         var RefundState =  $ && $('.js_refund_detail[data-seg="0"]');
        //
        //         if ($ && DetailButton && DetailButton.length) {
        //             logger.push('refund btn');
        //             //点击订单详情页“退票详情”
        //             // DetailButton.click();
        //             var clickEvent = document.createEvent('HTMLEvents');
        //             clickEvent.initEvent('click',false,true);
        //             var clickItem = document.getElementsByClassName('button button-ghost-blue button-primary j-selfsrv-item')[1];
        //             clickItem.dispatchEvent(clickEvent);
        //             clickItem.click();
        //
        //             window.tuipiao = true;
        //         }
        //         else if ($ && RefundState && RefundState.length) {
        //             logger.push('person');
        //             var clickEvent = document.createEvent('HTMLEvents');
        //             clickEvent.initEvent('click',false,true);
        //             var clickItem = document.getElementsByClassName('flight-passanger js_refund_detail')[0];
        //             clickItem.dispatchEvent(clickEvent);
        //             clickItem.click();
        //             window.tuipiao = true;
        //         }
        //     }
        //     return logger;
        //
        // });
        // loggerDebug('logger click', logger);
        //强制等待5秒后执行退票详情获取

        // var hybridRfdDetailProcessSize = page.evaluate(function () {
        //     var select = [];
        //     //获取处理进度,退票疑问和退票历史单以及继续退票
        //     var RefundDetail = $ && $('.allContons');
        //     if ($ && RefundDetail && RefundDetail.length ) {
        //         //隐藏退票疑问
        //         var tuipiaoQuestion = window.tuipiaoQuestion || $('.refund-section.j-faq-accordin');
        //         window.tuipiaoQuestion = tuipiaoQuestion;
        //         tuipiaoQuestion.hide();
        //
        //         var item = RefundDetail;
        //         var  position = item.offset();
        //         window.hybridFindEnd = new Date().getTime();
        //         select.push({
        //             position: position,
        //             item: item
        //         });
        //
        //
        //     }else{
        //         select.push('11111');
        //     }
        //     return select;
        // });
        // // loggerDebug('******************', RefundDetailSize);
        // //打印耗时，可以忽略
        // loggerTime('RefundDetailSizeDiv ', page.settings.iniTime);
        // console.log('hybridRfdDetailProcessSize '+ hybridRfdDetailProcessSize);
        if (dataInfo.selectDiv.hybridRfdDetailProcessSize && dataInfo.selectDiv.hybridRfdDetailProcessSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.hybridRfdDetailProcessSize, 'normalWithoutLeft');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRfdDetailProcess_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridRfdDetailProcess ',consoleData);
                taskCallback(null, 'success,  save hybridRfdDetailProcess');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridRfdDetailProcess_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridRfdDetailProcess',consoleData);
                taskCallback(null, 'fail,  save hybridRfdDetailProcess');
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridRfdDetailProcess_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridRfdDetailProcess',consoleData);
            taskCallback(null, 'fail,  save hybridRfdDetailProcess');
        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);
    },
};