
//截图任务task 按模块来写对应的函数


var utils = require('../../common/utils');
var computeSize = utils.computeSize;
var loggerInfo = utils.loggerInfo;
var loggerTime = utils.loggerTime;
var loggerDebug = utils.loggerDebug;
var computeUseTime = utils.computeUseTime;
var consoleData = {};
module.exports = {
    'getFlight':function (page, dataInfo) {
        var fightSize = page.evaluate(function () {
            window.hybridFindStart = new Date().getTime();
            var flightChange = $ && $('.flight-change');
            var orderState =  $ && $('.order-state');
            var select = [];
            if ($ && flightChange && flightChange.length
                && orderState && orderState.length) {
                //隐藏广告
                var guanggao = window.guanggao || $('#js_ads');
                window.guanggao = guanggao;
                guanggao.hide();
                //隐藏航班提示
                var hangbanLogo = window.hangbanLogo || $('.flight-section-container .airline-icon');
                window.hangbanLogo = hangbanLogo;
                hangbanLogo.hide();

                var item = flightChange;
                var position = item.offset();
                window.hybridFindEnd = new Date().getTime();
                select.push({
                    position: position,
                    // item: item.html(),

                });
                item = orderState;
                position = item.offset();
                select.push({
                    position: position,

                });
            }
            return select;
        });
        loggerDebug('fightSize', fightSize);
        if (fightSize && fightSize.length) {

            page.clipRect = computeSize(page, fightSize, 'flightChange');
            if (page.clipRect.height != 0) {
                page.render(dataInfo.PAGEPATH + 'hybridChgFlight_success.png', {
                    format: 'png',
                    quality: '50'
                });
                page.settings.pageCount++;
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('success:  save hybridChgFlight ',consoleData);
                // taskCallback(null, 'success, save hybridChgFlight');

            } else {
                page.clipRect = computeSize(page, [], 'whole');
                page.render(dataInfo.PAGEPATH + 'hybridChgFlight_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                loggerInfo('fail:  save hybridChgFlight ',consoleData);
                // taskCallback(null, 'fail, save hybridChgFlight');
            }
        } else {
            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridChgFlight_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridChgFlight ',consoleData);
            // taskCallback(null, 'fail, save hybridChgFlight');
        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    },
    'clickFun':function(page){
        console.log('in clickFun');
        var logger = page.evaluate(function () {
            var logger = [];
            //隐藏证件上传
            var maskDiv = $ && $('.mask');
            maskDiv.hide();
            var modalRemind =  $ && $('.modal.modal-remind');
            modalRemind.hide();
            //航变通知
            var flightChange = $ && $('.popup.popup-flight-change');
            flightChange.hide();
            //隐藏广告
            var guanggao = window.guanggao || $('#js_ads');
            window.guanggao = guanggao;
            guanggao.hide();
            //用于判断是否进入了改签详情页
            //用于判断是否进入了航变详情页
            logger.push('before click');
            var hangbianex=$ && $('[data-hpageid="10320608534"] .hh-title').html()=='航变详情';
            //先判断航变详情是否已打开，未打开的需先打开
            if(!hangbianex && !window.hangbian){
                //先确认订单详情页有航变详情按钮或者航班中有航变记录入口
                var DetailButton = $ && $('.js-showChanglist[data-speed="航班调整-卡片"]');
                if ($ && DetailButton && DetailButton.length) {
                    logger.push('click flight-passenger');
                    //点击订单详情页“航班调整”
                    var clickEvent = document.createEvent('HTMLEvents');
                    clickEvent.initEvent('click',false,true);
                    var clickItem = document.getElementsByClassName('flight-passanger js-showChanglist')[0];
                    clickItem.dispatchEvent(clickEvent);
                    clickItem.click();
                    window.hangbian = true;
                }

            }
            logger.push('after click');

            return logger;
        });
        loggerDebug('logger click', logger);

    },
    'gethybridChgDetail':function(page){
        console.log('in gethybridChangeDtl');
        var selectDiv = page.evaluate(function () {
            var select = {};
            var item;
            var position;
            // ChangeDetailSize
            //获取除开头部文案的卡片
            var ChangeDetail = $ && $('[data-hpageid="10320608534"] .operate-detail');
            //获取头部文案的卡片
            var ChangeDetailTitle = $ && $('[data-hpageid="10320608534"] .j-header.header-box');
            if ($ && ChangeDetail && ChangeDetail.length && ChangeDetailTitle && ChangeDetailTitle.length) {

                item = ChangeDetailTitle;
                position = item.offset();
                select.ChangeDetailSize = [];
                select.ChangeDetailSize.push({
                    position: position,
                    // item: item
                });
                item = ChangeDetail;
                position = item.offset();
                window.hybridFindEnd = new Date().getTime();
                select.ChangeDetailSize.push({
                    position: position,
                    // item: item

                });
            }
            // ChgDetailAfterSize
            //获取除开头部文案的卡片
            var ChgDetailAfter = $ && $('[data-hpageid="10320608534"] .flight');
            //获取头部文案的卡片
            var ChgDetailAfterTitle = $ && $('[data-hpageid="10320608534"] .j-header.header-box');

            if ($ && ChgDetailAfter && ChgDetailAfter.length && ChgDetailAfterTitle && ChgDetailAfterTitle.length) {

                item = ChgDetailAfterTitle;
                position = item.offset();
                select.ChgDetailAfterSize = [];
                select.ChgDetailAfterSize.push({
                    position: position,
                    // item: item
                });
                item = ChgDetailAfter;
                position = item.offset();
                window.hybridFindEnd = new Date().getTime();

                select.ChgDetailAfterSize.push({
                    position: position,
                    // item: item

                });
            }
            // hybridChgDetailBefore
            var ChgDetailBefore = $ && $('.operate-detail>.flight-section-container');
            //获取历史航班
            var ChgDetailBeforeTitle = $ && $('[data-hpageid="10320608534"] .operate-detail-history');

            if ($ && ChgDetailBefore && ChgDetailBefore.length && ChgDetailBeforeTitle && ChgDetailBeforeTitle.length) {

                item = ChgDetailBeforeTitle;
                position = item.offset();
                select.ChgDetailBeforeSize = [];
                select.ChgDetailBeforeSize.push({
                    position: position,
                    // item: item
                });
                item = ChgDetailBefore;
                position = item.offset();
                window.hybridFindEnd = new Date().getTime();
                select.ChgDetailBeforeSize.push({
                    position: position,
                    // item: item.html()

                });


            }

            return select;

        });
        loggerDebug('selectDiv', selectDiv);
        return selectDiv;
    },
    //整屏截图
    'hybridChgDetail': function  (page, dataInfo, taskCallback) {
        // console.log('PAGEPATH' + dataInfo.PAGEPATH);
        // loggerTime('start hybridChgDetail() ', page.settings.iniTime);
        // //启动的phantomjs中运行js脚本
        // var btn = page.evaluate(function () {
        //     //隐藏订单详情页广告
        //     var guanggao = window.guanggao || $('#js_ads');
        //     window.guanggao = guanggao;
        //     guanggao.hide();
        //     //用于判断是否进入了航变详情页
        //     var hangbianex=$ && $('[data-hpageid="10320608534"] .hh-title').html()=='航变详情';
        //     //先判断航变详情是否已打开，未打开的需先打开
        //     if(!hangbianex && !window.hangbian){
        //         //先确认订单详情页有航变详情按钮或者航班中有航变记录入口
        //         var DetailButton = $ && $('.js-showChanglist[data-speed="航班调整-卡片"]');
        //         if ($ && DetailButton && DetailButton.length) {
        //             //点击订单详情页“航班调整”
        //             var clickEvent = document.createEvent('HTMLEvents');
        //             clickEvent.initEvent('click',false,true);
        //             var clickItem = document.getElementsByClassName('flight-passanger js-showChanglist')[0];
        //             clickItem.dispatchEvent(clickEvent);
        //             clickItem.click();
        //             window.hangbian = true;
        //         }
        //
        //     }
        //
        // });
        // //强制等待5秒后执行航变详情获取
        // setTimeout(function () {
        //     consoleData = {
        //         env: dataInfo.env,
        //         orderid: dataInfo.orderid,
        //         number: dataInfo.orderidIndex,
        //     }
        //     console.log('***********************************************************ddddd' + JSON.stringify(consoleData));
        //     var ChangeDetailSize = page.evaluate(function () {
        //         var select = [];
        //         //获取除开头部文案的卡片
        //         var ChangeDetail = $('[data-hpageid="10320608534"] .operate-detail');
        //         //获取头部文案的卡片
        //         var ChangeDetailTitle = $ && $('[data-hpageid="10320608534"] .j-header.header-box');
        //         var item;
        //         var position;
        //         if ($ && ChangeDetail && ChangeDetail.length && ChangeDetailTitle && ChangeDetailTitle.length) {
        //
        //             item = ChangeDetailTitle;
        //             position = item.offset();
        //             select.push({
        //                 position: position,
        //                 item: item
        //             });
        //             item = ChangeDetail;
        //             position = item.offset();
        //             window.hybridFindEnd = new Date().getTime();
        //             select.push({
        //                 position: position,
        //                 item: item
        //
        //             });
        //
        //
        //         }else{
        //             select.push('11111');
        //         }
        //         return select;
        //     });
        //     // loggerDebug('******************', RefundDetailSize);
        //     //打印耗时，可以忽略
        //     loggerTime('ChangeDetailSizeDiv ', page.settings.iniTime);
        if (dataInfo.selectDiv.ChangeDetailSize && dataInfo.selectDiv.ChangeDetailSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.ChangeDetailSize, 'add');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridChgDetail_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridChgDetail ',consoleData);
                taskCallback(null, 'success,  save hybridChgDetail');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridChgDetail_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridChgDetail ',consoleData);
                taskCallback(null, 'fail,  save hybridChgDetail');
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridChgDetail_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridChgDetail ',consoleData);
            taskCallback(null, 'fail,  save hybridChgDetail');
        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);
        // }, 5*1000);


    },
    //只截取头部和航班新航班信息
    'hybridChgDetailAfter': function  (page, dataInfo, taskCallback) {
    // console.log('PAGEPATH' + dataInfo.PAGEPATH);
    // loggerTime('start hybridChgDetailAfter() ', page.settings.iniTime);
    // //启动的phantomjs中运行js脚本
    // var btn = page.evaluate(function () {
    //     //隐藏订单详情页广告
    //     var guanggao = window.guanggao || $('#js_ads');
    //     window.guanggao = guanggao;
    //     guanggao.hide();
    //     //用于判断是否进入了航变详情页
    //     var hangbianex=$ && $('[data-hpageid="10320608534"] .hh-title').html()=='航变详情';
    //     //先判断航变详情是否已打开，未打开的需先打开
    //     if(!hangbianex && !window.hangbian){
    //         //先确认订单详情页有航变详情按钮或者航班中有航变记录入口
    //         var DetailButton = $ && $('.js-showChanglist[data-speed="航班调整-卡片"]');
    //         if ($ && DetailButton && DetailButton.length) {
    //             //点击订单详情页“航班调整”
    //             var clickEvent = document.createEvent('HTMLEvents');
    //             clickEvent.initEvent('click',false,true);
    //             var clickItem = document.getElementsByClassName('flight-passanger js-showChanglist')[0];
    //             clickItem.dispatchEvent(clickEvent);
    //             clickItem.click();
    //             window.hangbian = true;
    //         }
    //
    //     }
    //
    // });
    // //强制等待5秒后执行航变详情获取
    // setTimeout(function () {
    //     consoleData = {
    //         env: dataInfo.env,
    //         orderid: dataInfo.orderid,
    //         number: dataInfo.orderidIndex,
    //     }
    //     console.log('***********************************************************ddddd' + JSON.stringify(consoleData));
    //     var ChangeDetailSize = page.evaluate(function () {
    //         var select = [];
    //         //获取除开头部文案的卡片
    //         var ChangeDetail = $ && $('[data-hpageid="10320608534"] .flight');
    //         //获取头部文案的卡片
    //         var ChangeDetailTitle = $ && $('[data-hpageid="10320608534"] .j-header.header-box');
    //         var item;
    //         var position;
    //         if ($ && ChangeDetail && ChangeDetail.length && ChangeDetailTitle && ChangeDetailTitle.length) {
    //
    //             item = ChangeDetailTitle;
    //             position = item.offset();
    //             select.push({
    //                 position: position,
    //                 item: item
    //             });
    //             item = ChangeDetail;
    //             position = item.offset();
    //             window.hybridFindEnd = new Date().getTime();
    //             select.push({
    //                 position: position,
    //                 item: item
    //
    //             });
    //
    //
    //         }else{
    //             select.push('11111');
    //         }
    //         return select;
    //     });
    //
    //     //打印耗时，可以忽略
    //     loggerTime('ChangeDetailSizeDiv ', page.settings.iniTime);

        if (dataInfo.selectDiv.ChgDetailAfterSize && dataInfo.selectDiv.ChgDetailAfterSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.ChgDetailAfterSize, 'add');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridChgDetailAfter_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridChgDetailAfter',consoleData);
                taskCallback(null, 'success,  save hybridChgDetailAfter');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridChgDetailAfter_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridChgDetailAfter ',consoleData);
                taskCallback(null, 'fail,  save hybridChgDetailAfter');
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridChgDetailAfter_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridChgDetailAfter',consoleData);
            taskCallback(null, 'fail,  save hybridChgDetailAfter');
        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);
    // }, 5*1000);


    },
    //只截取航变前航班信息
    'hybridChgDetailBefore': function  (page, dataInfo, taskCallback) {
        // console.log('PAGEPATH' + dataInfo.PAGEPATH);
        // loggerTime('start hybridChgDetailBefore() ', page.settings.iniTime);
        // //启动的phantomjs中运行js脚本
        // var btn = page.evaluate(function () {
        //     //隐藏订单详情页广告
        //     var guanggao = window.guanggao || $('#js_ads');
        //     window.guanggao = guanggao;
        //     guanggao.hide();
        //     //用于判断是否进入了航变详情页
        //     var hangbianex=$ && $('[data-hpageid="10320608534"] .hh-title').html()=='航变详情';
        //     //先判断航变详情是否已打开，未打开的需先打开
        //     if(!hangbianex && !window.hangbian){
        //         //先确认订单详情页有航变详情按钮或者航班中有航变记录入口
        //         var DetailButton = $ && $('.js-showChanglist[data-speed="航班调整-卡片"]');
        //         if ($ && DetailButton && DetailButton.length) {
        //             //点击订单详情页“航班调整”
        //             var clickEvent = document.createEvent('HTMLEvents');
        //             clickEvent.initEvent('click',false,true);
        //             var clickItem = document.getElementsByClassName('flight-passanger js-showChanglist')[0];
        //             clickItem.dispatchEvent(clickEvent);
        //             clickItem.click();
        //             window.hangbian = true;
        //         }
        //
        //     }
        //
        // });
        // //强制等待5秒后执行航变详情获取
        // setTimeout(function () {
        //     consoleData = {
        //         env: dataInfo.env,
        //         orderid: dataInfo.orderid,
        //         number: dataInfo.orderidIndex,
        //     }
        //     console.log('***********************************************************ddddd' + JSON.stringify(consoleData));
        //     var ChangeDetailSize = page.evaluate(function () {
        //         var select = [];
        //         //获取历史航班卡片
        //         var ChangeDetail = $ && $('.operate-detail>.flight-section-container');
        //         //获取历史航班
        //         var ChangeDetailTitle = $ && $('[data-hpageid="10320608534"] .operate-detail-history');
        //         var item;
        //         var position;
        //         if ($ && ChangeDetail && ChangeDetail.length && ChangeDetailTitle && ChangeDetailTitle.length) {
        //
        //             item = ChangeDetailTitle;
        //             position = item.offset();
        //             select.push({
        //                 position: position,
        //                 item: item
        //             });
        //             item = ChangeDetail;
        //             position = item.offset();
        //             window.hybridFindEnd = new Date().getTime();
        //             select.push({
        //                 position: position,
        //                 item: item
        //
        //             });
        //
        //
        //         }else{
        //             select.push('11111');
        //         }
        //         return select;
        //     });
        //
        //     //打印耗时，可以忽略
        //     loggerTime('ChangeDetailSizeDiv ', page.settings.iniTime);

        if (dataInfo.selectDiv.ChgDetailBeforeSize && dataInfo.selectDiv.ChgDetailBeforeSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, dataInfo.selectDiv.ChgDetailBeforeSize, 'add');
            if (page.clipRect.height != 0) {
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridChgDetailBefore_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save hybridChgDetailBefore',consoleData);
                taskCallback(null, 'success,  save hybridChgDetailBefore');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'hybridChgDetailBefore_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save hybridChgDetailBefore ',consoleData);
                taskCallback(null, 'fail,  save hybridChgDetailBefore');
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'hybridChgDetailBefore_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save hybridChgDetailBefore',consoleData);
            taskCallback(null, 'fail,  save hybridChgDetailBefore');
        }

        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);
        // }, 5*1000);


    },

};