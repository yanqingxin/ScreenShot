//计算页面截图所需的基础数据信息

//截图可选环境url
var url395 = 'http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid={Orderid}';
var url396 = 'http://m.ctrip.fat396.qa.nt.ctripcorp.com/webapp/flight/orderdetail/orderdetail.html?oid={Orderid}';

//截图所需基础数据，必传
var dataService = {
    //截图所需url
    urls: [],
    //日志展示用的环境
    envs: [],
    //每个page存放路径
    pagefiles: [],
    //截图的订单号
    orderids: [],
    //截图的第几个订单号
    orderidIndexs: [],
    //log保存路径
    LOGPATH: '',
    //截图page保存路径
    PAGEPATH:'',
    //保存整图的时候，页面名称
    wholePageName: 'hybridChgDetail',
};

function getBaseData(inputData) {
    var pageFile1 = '';
    var pageFile2 = '';

    var urlHybridDetail1 = '';
    var urlHybridDetail2 = '';
    //设置log及图片保存位置  无需修改
    if(dataService  && dataService.LOGPATH ===''){
        dataService.LOGPATH = inputData.LOGPATH;
    }
    if(dataService &&  dataService.PAGEPATH ===''){
        dataService.PAGEPATH = inputData.PAGEPATH;
    }
    //根据可选环境，生成截图所需访问的urls,
    if (inputData.env1 == 'fat395') {
        urlHybridDetail1 = url395;
        pageFile1 = dataService.PAGEPATH + '-env1-' + inputData.env1 + '/';
    } else if (inputData.env1 == 'fat396') {
        urlHybridDetail1 = url396;
        pageFile1 = dataService.PAGEPATH + '-env1-' + inputData.env1 + '/';
    }
    if (inputData.env2 == 'fat395') {
        urlHybridDetail2 = url395;
        pageFile2 = dataService.PAGEPATH + '-env2-' + inputData.env2 + '/';
    } else if (inputData.env2 == 'fat396') {
        urlHybridDetail2 = url396;
        pageFile2 = dataService.PAGEPATH + '-env2-' + inputData.env2 + '/';
    }
    if(inputData.orderid && inputData.orderid.length) {
        for (var i = 0; i < inputData.orderid.length; i++) {
            var url = urlHybridDetail1.replace(/\{Orderid\}/, inputData.orderid[i]);
            dataService.urls.push(url);
            dataService.pagefiles.push(pageFile1);
            dataService.orderids.push(inputData.orderid[i]);
            dataService.envs.push(1 + ' ' + inputData.env1);
            //同一个订单号 算一个orderid,此处orderidindex同orderid保持一致，orderid相同的orderidIndex相同
            dataService.orderidIndexs.push(i + 1);
            url = urlHybridDetail2.replace(/\{Orderid\}/, inputData.orderid[i]);
            dataService.urls.push(url);
            dataService.pagefiles.push(pageFile2);
            dataService.orderids.push(inputData.orderid[i]);
            dataService.envs.push(2 + ' ' + inputData.env2);
            //同一个订单号 算一个orderid,此处orderidindex同orderid保持一致，orderid相同的orderidIndex相同
            dataService.orderidIndexs.push(i + 1);
        }
    }
    console.log('dataService ' + JSON.stringify(dataService));

    return dataService;
}

module.exports = {
    getBaseData: getBaseData
};