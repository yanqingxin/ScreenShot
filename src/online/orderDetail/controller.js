﻿
var config = require('./config');
var moduleTask = require('./moduleTask');
var dataService = require('./dataService');
module.exports = {
    //获取页面截图所需的基础数据信息
    getModuleData: function (inputData) {
        return {
            baseData: dataService.getBaseData(inputData),
            model : 1  //无效数据，待用
        };
    },
    //截图任务task
    taskHash: moduleTask,
    //截取模块依赖的接口 需监控的css、js
    config: config
};
