
//截图任务task 按模块来写对应的函数


var utils = require('../../common/utils');
var computeSize = utils.computeSize;
var loggerInfo = utils.loggerInfo;
var loggerTime = utils.loggerTime;
var loggerDebug = utils.loggerDebug;
var computeUseTime = utils.computeUseTime;
var consoleData = {};

module.exports = {
    'getbackChangeDetail':function(page){
        var selectDiv =  page.evaluate(function () {
            var select = {};
            var backChange = document.querySelectorAll('.tooltip.tooltip_table.tab_arrow.tgq-pannel');
            var item, position;
            if(backChange && backChange.length){
                item = backChange[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height, //|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    backChange[0].style.display = '';
                }
            }
            return select;


        });
        return selectDiv;
    },
    'backChangeDetail':function(page, dataInfo, taskCallback) {
        console.log('start backChangeDetail() useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');
        //优惠券信息
        var backChangeSize = page.evaluate(function () {
            var select = [];
            var backChange = document.querySelectorAll('.tooltip.tooltip_table.tab_arrow.tgq-pannel');
            var item, position;
            if (backChange && backChange.length) {
                //获取需要截取模块的位置信息
                item = backChange[0];

                position = {
                    top: item.getBoundingClientRect().top,//位置计算： 该点距离顶部的位置
                    left: item.getBoundingClientRect().left,
                    width: item.getBoundingClientRect().width,//位置计算： 该点宽度
                    height: item.getBoundingClientRect().height, //|| page.viewportSize.height////位置计算： 该点高度
                };
                if(!position.height){
                    backChange[0].style.display = '';
                }
                //保存要返回的数据
                select.push({
                    //位置信息
                    position: position,

                });


            }
            return select;
        });
        // console.log('backChangeSize:' + JSON.stringify(backChangeSize))
        if (backChangeSize && backChangeSize.length) {
            //返回有数据
            //设置phantomjs截图位置
            page.clipRect = computeSize(page, backChangeSize, 'normal');
            if (page.clipRect.height != 0) {
                //保存图片的名称，路径使用dataInfo.PAGEPATH 名称为页面名称+“_sunccess”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'backChangeDetail_success.png', {
                    format: 'png',
                    quality: '50'
                });
                //计算截图模块的页面个数，成功时+1，必须统计节点
                page.settings.pageCount++;
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };
                //打印格式 截图结果+图片名称+对应的基础信息 所需数据在consoleData中
                loggerInfo('success:  save backChangeDetail',consoleData);
                taskCallback(null, 'success, save backChangeDetail');
            } else {
                //模块高度为0，该div未显示
                page.clipRect = computeSize(page, [], 'whole');
                //保存图片的名称，  路径使用dataInfo.PAGEPATH 名称为页面名称+“_fail”（图片名称与函数名称一致）
                page.render(dataInfo.PAGEPATH + 'backChangeDetail_fail.png', {
                    format: 'png',
                    quality: '50'
                });
                //需要打印的信息，用于页面显示
                consoleData = {
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                };

                loggerInfo('fail:  save backChangeDetail ',consoleData);
                taskCallback(null, 'fail, save backChangeDetail');
            }
        } else {

            page.clipRect = computeSize(page, [], 'whole');
            page.render(dataInfo.PAGEPATH + 'backChangeDetail_fail.png', {
                format: 'png',
                quality: '50'
            });
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerInfo('fail:  save backChangeDetail ',consoleData);
            taskCallback(null, 'fail, save backChangeDetail');

        }
        consoleData = {
            pageCount: page.settings.pageCount,
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        loggerDebug(' ',consoleData);

    }
};