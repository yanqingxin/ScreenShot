module.exports = {
    "env": {
        "browser": true,
        "node": true,
        "phantomjs": true,
        "es6": true,
    },
    "extends": "eslint:recommended",
    "globals": {
        "$": true,
        "Promise": true,
    },
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
        },
        "sourceType": "module"
    },
    "parser": "babel-eslint",
    "rules": {

        "no-console": "off",
        "no-unused-vars": ["warn", { "vars": "all", "args": "after-used", "ignoreRestSiblings": false }],
        "no-irregular-whitespace": "off",
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};