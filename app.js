//var orderid = [2185907150,2185906009,2185906001,2185886827,2185886821,2185886795,2185886730,2185886728,2185871436,2185864106,2185864059,2185863583,2185863572,2185863569,2185863562,2185860182,2185839288,2185839287,2185828635,2185828481,2185828328,2185828261,2185828189,2185818457,2185817251,2185816765,2185816695,2185816192,2185814404,2185814292,2185812227,2185806050,2185805904,2185805610,2185790837,2185789647,2185789644,2185789520,2185789516,2185789419,2185785292,2185785291,2185785284,2185785283,2185785152,2185737474,2185737470,2185735146,2185734825,2185733930,2185726380,2185722319,2185718625,2185718622,2160997922,2160993867,2160993866,2160993799,2160986137,2160986130,2160986100,2160985978,2160985948,2160985885,2160985884,2160985650,2160985649,2160984871,2160984870,2160984690,2160984576,2160984454,2160984419,2160984318,2160984213,2160984212,2160983792,2160983790,2160982590,2160979207,2160978797,2160978704,2160978474,2160978280,2160978160,2160966636,2160961398,2160954394,2160954393,2160912482,2160912464,2160912457,2160912446,2160912314,2160912170,2160912169,2160900877,2160900831,2160900830,2160900822];
var webPage = require('webpage');
//清除缓存
//webPage.clearHttpCache();
var system = require('system');
var async = require('async');
var _ = require('lodash');
var fs = require('fs');
var count = 0;
var page = webPage.create();
var curTime = Date.now();
/*
var env1 = system.args[1] || 'fat395';
var env2 = system.args[2] || 'fat396';
var ROOTPATH = system.args[3];
var date = system.args[4];
var datekey = system.args[5];
var uid = system.args[6] || '18817840312'//'18817840312';
var pwd = system.args[7] || '123456'//'123456'
var orderidlist = system.args[8] || '3020375561'//'3020375561'; 3020567324
var requirePath = system.args[9];//指定截取渠道
//截取模块
var selectStr = system.args[10];
//登录cookie
var cookieLogin = system.args[11] || '88B04A49413AFCBE9D1A973C8BBE22FEC51441CAFD02FB62921D19A7A8D293DD';
*/
//接收待解析数据
var inputInfo = system.args[1];
inputInfo = inputInfo.split(';');
//解析后的输入数据
console.log('inputInfo' + JSON.stringify(inputInfo));
var inputData = {};
var workingTime;
var channelCookieKey = {
    'online':'ticket_ctrip',
    'app':'cticket',
    'offline': 'offlineTicket',
};
for(var item in inputInfo){
    inputData[inputInfo[item].split('=')[0]] = inputInfo[item].split('=')[1];
}

inputData.orderid = inputData.orderidlist.indexOf(',') ? inputData.orderidlist.split(',') : inputData.orderidlist;
//var userinfo = { UID: uid, PWD: pwd };
//截取模块数组
inputData.selectData = inputData.selectStr.indexOf(',') ? inputData.selectStr.split(',') : inputData.selectStr;
//保存图片路径
inputData.LOGPATH = inputData.IMAGEPATH + inputData.date;
inputData.PAGEPATH = inputData.IMAGEPATH + inputData.datekey;
console.log('------cookieLogin' + inputData.cookieLogin);


//补偿登录cookie未获取的情况
if (inputData  && inputData.cookieLogin =='') {
    //属性值为空
    inputData.cookieLogin = '88B04A49413AFCBE9D1A973C8BBE22FE85246FDED561D0852156C23F94F88595';
}else if(!inputData.cookieLogin ){
    console.log('----------ddd--------------');
    //无该属性
    inputData.cookieLogin = '88B04A49413AFCBE9D1A973C8BBE22FE85246FDED561D0852156C23F94F88595';
}
// console.log('------cookieLogin' + inputData.cookieLogin);
// console.log('------inputData' + JSON.stringify(inputData));
//计算div，计算截图尺寸及位置
var utils = require('./src/common/utils');
var computeSize = utils.computeSize;
var loggerInfo = utils.loggerInfo;
var loggerTime = utils.loggerTime;
var loggerDebug = utils.loggerDebug;
var computeUseTime = utils.computeUseTime;
var getCallbackInfo = utils.getCallbackInfo;

//获取其他模块路径
var requirePath = './src/' + inputData.requirePath;
// console.log('requirePath:' + requirePath);

//获取基础数据
var controller = require(requirePath);

var baseDataInfo;
//执行任务urls
var urls ;

var orderids;
var orderidIndexs;
var pagefiles;

var envs;
if(system.args.length > 2){
    console.log('--------------------------nodePhanom----------------------');
    //参数大于2 重新生成url
    var result = JSON.parse(system.args[2]);
    // console.log(result);
    baseDataInfo = controller.getModuleData(inputData, result).baseReplayData;
    // console.log('++++++'+JSON.stringify(baseDataInfo));
    // console.log('*****' + JSON.stringify(baseDataInfo.urls));
    //执行任务urls
    urls = baseDataInfo.urls;

    orderids = baseDataInfo.orderids;
    orderidIndexs = baseDataInfo.orderidIndexs;
    pagefiles = baseDataInfo.pagefiles;
    envs = baseDataInfo.envs;
}else{
    //获取基础数据
    baseDataInfo = controller.getModuleData(inputData).baseData;
    //执行任务urls
    urls = baseDataInfo.urls;

    orderids = baseDataInfo.orderids;
    orderidIndexs = baseDataInfo.orderidIndexs;
    pagefiles = baseDataInfo.pagefiles;
    envs = baseDataInfo.envs;

}



var cssData = controller.config.cssData;
var jsData = controller.config.jsData;
//模块任务hash
var taskHash = controller.taskHash;
//需要判断的ajax接口
var ajaxHash = controller.config.ajaxHash;
var consoleData = {};
// console.log('baseDataInfo', JSON.stringify(baseDataInfo));

// console.log('LOGPATH' + inputData.LOGPATH);

// console.log('selectData: ' + inputData.selectData);
page.settings.userAgent = 'WebKit/534.46 Mobile/9A405 Safari/7534.48.3';
page.viewportSize = { width: 414, height: 3000 };
// phantom.addCookie({
//     name: 'ticket_ctrip',
//     value: '0qhbQpMZk+kd79FDSDoZa6rgT/qTuN/pgqQHMcaiTNFO9SfFwJHtVV3bOyC3r0KQf3NuaGanRgBBlESBJBvhk9A0RmFljBYCPH2mjtAPypKFujB1fU1TKJS9IQILQvC4iRYvCr1gJXxMfh+0a/5ykENpWh/i22tULcwkAlaFXAAwxwWxBjz4DQD5dfaMS7CK2PFDPQ7q/NnZez7jFtLE4PcyNBW5fbAVHtIXkTiYD6gnoAPOC/o+ApAEbnvmQn1t/iDzygUZ8o6FXMfpyA6T71chlB3U1k0XSll8g7GeOf3Lc5rplMvDcg==',
//     domain: '.qa.nt.ctripcorp.com',//'.ctrip.com',  测试 .qa.nt.ctripcorp.com
//     path: '/',
//     secure: false,
//     httponly: false,
//     expires: Date.now() + (1000 * 60 * 60 * 24 * 5)
// });

//重定向console到文件
Object.defineProperty(console, 'toFile', {
    get: function () {
        return console.__file__;
    },
    set: function (val) {
        if (!console.__file__ && val) {
            console.__log__ = console.log;
            console.log = function () {
             
                var msg = '';
                for (var i = 0; i < arguments.length; i++) {
                    msg += ((i === 0) ? '' : ' ') + arguments[i];
                }
                if (msg) {
                    fs.write(console.__file__, msg + '\r\n', 'a');
                }
            };
        }
        else if (console.__file__ && !val) {
            console.log = console.__log__;
        }
        console.__file__ = val;
    }
});


var logPath = inputData.LOGPATH;
//重定向console到本地
// console.toFile = logPath + '/phantomjsLog_' + inputData.datekey + '.txt';
//恢复到控制台
console.toFile = '';

var mapLimitNum = baseDataInfo.wholePageName=='hybridOrderDetail' ? 7:1;
mapLimitNum = 1;
console.log('mapLimitNum: '+ mapLimitNum);

//捕获页面js错误
phantom.onError = function(msg, trace) {
    var msgStack = ['PHANTOM ERROR: ' + msg];
    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function(t) {
            msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function +')' : ''));
        });
    }
    console.error(msgStack.join('\n'));
};


console.log('selectData ' + JSON.stringify(inputData.selectData));

//根据输入的模块，得到需要执行的任务
var tasks = inputData.selectData.map(function (type) {
    return taskHash[type];
});


console.log('-------------------tasks' + JSON.stringify(tasks));

if(baseDataInfo.wholePageName == 'hybridRbkDetail'
    || baseDataInfo.wholePageName == 'hybridRfdDetail'
    || baseDataInfo.wholePageName == 'hybridChgDetail')
{
    inputData.selectData.push('getFlight');
    loggerDebug('********************inputData.selectData', inputData.selectData);
}
//通过得到的task算出需要验证的url
var urlArrResHash = {};

// 合并&去重
var urlArr = inputData.selectData
    .reduce(function (url, curSel) {
        if (ajaxHash[curSel]) {
            url = url.concat(ajaxHash[curSel]);
        }
        return url;
    }, [])
    .reduce(function (urlArr, item) {
        if (!urlArrResHash[item.url]) {
            urlArrResHash[item.url] = item.url;
            urlArr.push(item);
        }
        return urlArr;
    }, []);
//查看是否等待接口返回
var isWaitPortResInfo = {
    waitTime:0,
    isWait: false,
};
urlArr.forEach(function(item){
    if(item.withoutJudgePort){
        isWaitPortResInfo.isWait = true;
        isWaitPortResInfo.waitTime = item. withoutJudgePort;
    }
});
//最终判断url
loggerDebug('last select url: ', urlArr);
function app() {
    if (inputData && inputData.cookieLogin) {
        console.log('login');
        if(inputData.cookieChannel){

            //赋值登录cookie
            console.log('setCookie');
            console.log('channelCookieKey[inputData.cookieChannel]:' + channelCookieKey[inputData.cookieChannel.trim()]);
            if(inputData.cookieChannel.trim() == 'online'){
                inputData.cookieLogin = decodeURIComponent(inputData.cookieLogin);
                console.log('inputData.cookieLogin:'+inputData.cookieLogin);
            }
            phantom.addCookie({
                name: channelCookieKey[inputData.cookieChannel.trim()],
                value: inputData.cookieLogin,
                domain: '.qa.nt.ctripcorp.com',//'.ctrip.com',  测试 .qa.nt.ctripcorp.com
                path: '/',
                secure: false,
                httponly: false,
                expires: curTime + (1000 * 60 * 60 * 24 * 5)
            });


        }else{
            phantom.addCookie({
                name: 'cticket',
                value: inputData.cookieLogin,
                domain: '.qa.nt.ctripcorp.com',//'.ctrip.com',  测试 .qa.nt.ctripcorp.com
                path: '/',
                secure: false,
                httponly: false,
                expires: curTime + (1000 * 60 * 60 * 24 * 5)
            });
        }

        //启动截图
        startAppTask();
    } else {
        page.open(urls[0], function () {
            openLoginPage();
        });
    }
}

function openLoginPage() {
    var taskCount = 20;
    var taskFn = function () {
        taskCount--;
        var pageResult = page.evaluate(function () {
            return document.getElementById('username');
            // || ($ && $('.hh-title').length > 0)
        });
        if (!taskCount) {
            console.log('fail: task end, exit app. Please try again');

            page.render(inputData.LOGPATH + '/loginfail_' + inputData.datekey + '.png', { format: 'png', quality: '50' });
            //callback(null, 'fail: login');
            phantom.exit();
        }
        else {
            if (pageResult) {
                //console.log('success: open login url');
                clearTimeout(timer);
                // startLogin(userinfo);
            }
            else {
                console.log('wait: for login');
                timer = setTimeout(taskFn, 1000);
            }
        }
    };
    var timer = setTimeout(taskFn, 1000);
}

// 每个页面的任务控制器
function pageTask(page, arrRes, res, dataInfo, callback) {

    // console.log('pageTask--------------');
    // console.log('arrRes:'+JSON.stringify(arrRes))
    var flag = true;
    if(isWaitPortResInfo && isWaitPortResInfo.isWait && isWaitPortResInfo.waitTime){
        setTimeout(function(){
            //进入时 未隐藏模块截图
            wholePageShot(page, dataInfo);
            if(taskHash['get'+ baseDataInfo.wholePageName]){
                dataInfo.selectDiv = taskHash['get'+ baseDataInfo.wholePageName](page);
                loggerDebug('dataInfo.selectDiv', dataInfo.selectDiv);
            }
            async.mapLimit(tasks, mapLimitNum, function (task, taskCallback) {

                console.log('task' + JSON.stringify(task));
                task(page, dataInfo, taskCallback);
            },function(err,result){
                console.log('result' + result);
                shotCallback(page, dataInfo, callback);
            });
        }, isWaitPortResInfo.waitTime);
    }else{
        arrRes.forEach(function (item) {

            if(!item.isResSucc && item.urlRe.test(res.url) && (res.contentType =='application/json;charset=utf-8' || res.contentType === item.resType)){

                item.isResSucc = item.urlRe.test(res.url); //&& res.status =='200';
                //console.log('received: ' + JSON.stringify(res, undefined, 4));

                consoleData = {
                    receivedUrl: res.url,
                    index: page.settings.index,
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    number: dataInfo.orderidIndex,
                    useTime: computeUseTime(Date.now(), page.settings.iniTime),
                    contentType: res.contentType,
                    status: res.status,
                    stage: res.stage
                };
                loggerDebug('received ',consoleData);


                arrRes.forEach(function (item) {
                    consoleData = {
                        url: item.url,
                        status: item.isResSucc,
                        index: page.settings.index,
                        env: dataInfo.env,
                        orderid: dataInfo.orderid,
                        number: dataInfo.orderidIndex,
                        useTime: computeUseTime(Date.now(), page.settings.iniTime),

                    };
                    loggerDebug('received ',consoleData);

                    if (!item.isResSucc) flag = false;
                });
                console.log(flag +' index: '+page.settings.index);
                //
                if (flag) {

                    //延时1s后执行
                    setTimeout(function () {
                        //进入时 未隐藏模块截图

                        wholePageShot(page, dataInfo);
                        //根据输入模块 按任务截图

                        // tasks.forEach(function (task) {
                        //     task(page, dataInfo);
                        // });
                        //详情页 先将div获取后再跑job
                        if(baseDataInfo.wholePageName=='hybridDetail'){
                            dataInfo.selectDiv = taskHash['get'+ baseDataInfo.wholePageName](page);
                            //截图代码走task管理
                            async.mapLimit(tasks, mapLimitNum, function (task, taskCallback) {

                                console.log('task' + JSON.stringify(task));
                                task(page, dataInfo, taskCallback);
                            },function(err,result){
                                console.log('result' + result);
                                shotCallback(page, dataInfo, callback);
                            });
                            loggerDebug('dataInfo.selectDiv', dataInfo.selectDiv);
                        }else if(baseDataInfo.wholePageName == 'hybridRbkDetail' || baseDataInfo.wholePageName == 'hybridRfdDetail' || baseDataInfo.wholePageName == 'hybridChgDetail'){
                            //改签详情需要延时
                            console.log('start');

                            taskHash.getFlight(page, dataInfo);

                            taskHash.clickFun(page);
                            setTimeout(function(){
                                dataInfo.selectDiv = taskHash['get'+ baseDataInfo.wholePageName](page);
                                loggerDebug('dataInfo.selectDiv', dataInfo.selectDiv);
                                //截图代码走task管理
                                async.mapLimit(tasks, mapLimitNum, function (task, taskCallback) {

                                    console.log('task' + JSON.stringify(task));
                                    task(page, dataInfo, taskCallback);
                                },function(err,result){
                                    console.log('result' + result);
                                    shotCallback(page, dataInfo, callback);
                                });
                            },5*1000);
                        }else{
                            // taskHash.clickFun(page);
                            //  //退票
                            //  //截图代码走task管理
                            // setTimeout(function(){
                            //     dataInfo.selectDiv = taskHash['get'+ baseDataInfo.wholePageName](page);
                            //     loggerDebug('dataInfo.selectDiv', dataInfo.selectDiv);
                            //     //截图代码走task管理
                            //     async.mapLimit(tasks, mapLimitNum, function (task, taskCallback) {
                            //
                            //         console.log('task' + JSON.stringify(task));
                            //         task(page, dataInfo, taskCallback);
                            //     },function(err,result){
                            //         console.log('result' + result);
                            //         shotCallback(page, dataInfo, callback);
                            //     });
                            // },5*1000);

                            async.mapLimit(tasks, mapLimitNum, function (task, taskCallback) {

                                console.log('task' + JSON.stringify(task));
                                task(page, dataInfo, taskCallback);
                            },function(err,result){
                                console.log('result' + result);
                                shotCallback(page, dataInfo, callback);
                            });

                        }

                        //截图后，统计截取情况，调用callback函数
                        // setTimeout(function(){
                        //     shotCallback(page, dataInfo, callback)
                        // }, 10*1000);
                    }, 10000);
                }
            }

        });
    }

}

function wholePageShot(page, dataInfo) {
    loggerTime('start wholePageShot()', page.settings.iniTime);
    /*
    var body = page.evaluate(function () {
        //添加css
        var oLink = document.createElement('link');
        oLink.rel = "stylesheet";
        oLink.href = 'http://webresource.c-ctrip.com/ResH5FlightOnline/R3/swift-20170215142737/assets/styles/spa.css?' + new Date().getTime();
        document.body.appendChild(oLink);
        return document.body.innerHTML;
    });
    console.log('*******************************body*******************************');
    //console.log(body);
    console.log('*******************************body*******************************');
    */
    //全图
    var WHOLEPAGE = inputData.LOGPATH + '/' + dataInfo.orderid + '_' + dataInfo.datekey + '_env' + dataInfo.env + '_';
    console.log('WHOLEPAGE: ' + WHOLEPAGE);
    page.clipRect = computeSize(page, [], 'whole');
    page.render(WHOLEPAGE + baseDataInfo.wholePageName + '_wholePage.png', {
        format: 'png',
        quality: '50'
    });
    consoleData = {
        env: dataInfo.env,
        number: dataInfo.orderidIndex,
        useTime: computeUseTime(Date.now(), page.settings.iniTime),
    };
    loggerDebug('save wholepage ',consoleData);
}

function shotCallback(page, dataInfo, callback) {
    loggerTime('start shotCallback()', page.settings.iniTime);
    loggerDebug('pageCount: ', page.settings.pageCount);
    loggerDebug('inputData.selectData: ', inputData.selectData);
    if (page.settings.pageCount == inputData.selectData.length) {
        page.clipRect = computeSize(page, [], 'whole');
        page.render(dataInfo.PAGEPATH  + '_wholePage_success.png', {
            format: 'png',
            quality: '50'
        });
        loggerTime('save success page', page.settings.iniTime);
        page.close();
        consoleData = {
            env: dataInfo.env,
            number: dataInfo.orderidIndex,
        };
        loggerDebug(baseDataInfo.wholePageName + 'wholePage_Success ', consoleData);
        loggerInfo('finish: --------------------', consoleData);
        workingTime = (Date.now() - curTime) / 1000;
        //清除超时定时时间
        clearTimeout(page.settings.timer);
        consoleData = {
            env: dataInfo.env,
            orderid: dataInfo.orderid,
            number: dataInfo.orderidIndex,
            workingTime: computeUseTime(Date.now(), curTime),
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        callback(null, getCallbackInfo('success: ', consoleData));
    } else {
        page.clipRect = computeSize(page, [], 'whole');
        page.render(dataInfo.PAGEPATH  + '_wholePage_fail.png', {
            format: 'png',
            quality: '50'
        });
        loggerTime('save fail page', page.settings.iniTime);
        page.close();
        consoleData = {
            env: dataInfo.env,
            orderid: dataInfo.orderid,
            number: dataInfo.orderidIndex,
        };
        loggerInfo(baseDataInfo.wholePageName + '_wholePage_Fail', consoleData);
        loggerInfo('finish: --------------------', consoleData);

        workingTime = (Date.now() - curTime) / 1000;
        //清除超时定时时间
        clearTimeout(page.settings.timer);
        consoleData = {
            env: dataInfo.env,
            orderid: dataInfo.orderid,
            number: dataInfo.orderidIndex,
            workingTime: computeUseTime(Date.now(), curTime),
            useTime: computeUseTime(Date.now(), page.settings.iniTime),
        };
        callback(null, getCallbackInfo('fail: ', consoleData));
    }
}

function handle_page(dataInfo, callback) {
    loggerDebug('datainfo: ', dataInfo);

    var PAGEPATH = dataInfo.pagefile + dataInfo.orderid + '_' + dataInfo.datekey + '_';
    dataInfo.PAGEPATH= PAGEPATH;
    console.log('dataInfo.PAGEPATH ' + dataInfo.PAGEPATH);
    count++;
    var curPage = webPage.create();
    var maxTime = Math.max((inputData.selectData.length * 60), 200) * 1000;
    if(inputData && inputData.cookieChannel && inputData.cookieChannel == 'online'){
        curPage.settings.clearMemoryCaches = false;
    }else{
        curPage.settings.clearMemoryCaches = true;
        //添加后记录了js的日志
        curPage.clearMemoryCache();
    }

    curPage.settings.index = count;
    curPage.settings.pageCount = 0;
    curPage.settings.userAgent = 'WebKit/534.46 Mobile/9A405 Safari/7534.48.3';
    curPage.settings.iniTime = Date.now();
    curPage.orderid = dataInfo.orderid;
    curPage.env = dataInfo.env;
    console.log('pageIniTime: ' + curPage.settings.iniTime  + 'ms');
    // setTimeout(function () {
    //     console.log('save page');
    //     curPage.render( 'success.png',{
    //         format: 'png',
    //         quality: '50'
    //     })
    // }, 1000*10);

    consoleData = {
        env: dataInfo.env,
        orderid: dataInfo.orderid,
        number: dataInfo.orderidIndex
    };
    loggerInfo('wait: for open url ', consoleData);

    //超时设置，超过时间 停止task
    curPage.settings.timer = setTimeout(function () {
        curPage.render(dataInfo.PAGEPATH + baseDataInfo.wholePageName +'_timeout.png', {
            format: 'png',
            quality: '50'
        });
        consoleData = {
            env: dataInfo.env,
            orderid: dataInfo.orderid,
            number: dataInfo.orderidIndex,
        };

        loggerInfo('fail: reason: timeout', consoleData);
        loggerInfo('finish: --------------------', consoleData);
        curPage.close();
        workingTime = (Date.now() - curTime) / 1000;

        consoleData = {
            env: dataInfo.env,
            orderid: dataInfo.orderid,
            number: dataInfo.orderidIndex,
            workingTime: computeUseTime(Date.now(), curTime),
            useTime: computeUseTime(Date.now(), curPage.settings.iniTime),
        };
        callback(null, getCallbackInfo('fail: reason: timeout, ', consoleData));
    }, maxTime);



    curPage.viewportSize = {
        width: 414,
        height: 3000
    };
    //深拷贝需要判断的url，这样不会修改外部配置
    var arrRes = _.cloneDeep(urlArr);
    // arrRes = [{
    //     //依赖的url
    //     url: 'OrderDetailSearchV2',
    //     //url对应的正则
    //     urlRe: /OrderDetailSearchV2/,
    //     //是否发起请求，暂时未用该数据
    //     isReqSucc: false,
    //     //是否接收到返回 默认false
    //     isResSucc: false
    // }]
    //捕获页面js错误
    curPage.onError = function(msg, trace) {

        var msgStack = ['ERROR: ' + msg];

        if (trace && trace.length) {
            msgStack.push('TRACE:');
            trace.forEach(function(t) {
                msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
            });
        }

        console.error(msgStack.join('\n'));

    };

    curPage.onResourceRequested = function (requestData, request) {
        //console.log('requested: ' + JSON.stringify(requestData, undefined, 4));
        var baseUrl = 'http://dst59597/YLS.Web/Resources/mock/orderdetail/';
        //var baseUrl = 'http://10.2.74.69/FlightTest/Resources/mock/orderdetail/';
        var hostname = requestData.url.match(/\/\/.+?\//)[0];

        // if (!/ctrip/.test(hostname) || /ubt/.test(hostname) || /socket/.test(hostname) || /pages.dev.sh.ctriptravel.com/.test(hostname)) {
        //     //pages.dev.sh.ctriptravel.com 会有配置文件超时影响详情loading
        //     // console.log(page.settings.index + 'req Aborting: ' + JSON.stringify(requestData, undefined, 4));
        //     request.abort();
        // } else {
        //     /*
        //     console.log('requested: ' + requestData.url +
        //            ' index: ' + curPage.settings.index +
        //            ', env: ' + env +
        //            ', orderid: ' + ord +
        //            ', number: ' + orderidIndex +
        //            ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's');
        //        */
        // }
        if(system.args.length > 2){
            //走replay
            if (/ubt/.test(hostname) || /socket/.test(hostname) || /google-analytics/.test(hostname) || /\.gif/.test(hostname)) {
                //pages.dev.sh.ctriptravel.com 会有配置文件超时影响详情loading
                // console.log(page.settings.index + 'req Aborting: ' + JSON.stringify(requestData, undefined, 4));
                request.abort();
            }
        }else{
            //不走replay
            if (!/ctrip/.test(hostname) || /ubt/.test(hostname) || /socket/.test(hostname) || /pages.dev.sh.ctriptravel.com/.test(hostname)) {
                //pages.dev.sh.ctriptravel.com 会有配置文件超时影响详情loading
                // console.log(page.settings.index + 'req Aborting: ' + JSON.stringify(requestData, undefined, 4));
                request.abort();
            }
        }


        /*
        if (/ctrip/.test(hostname) && !/ubt/.test(hostname)) {
            console.log('requested: ' + requestData.url +
                    ' index: ' + curPage.settings.index +
                    ', env: ' + env +
                    ', orderid: ' + ord +
                    ', number: ' + orderidIndex +
                    ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's');
        }*/

        // curPage.addCookie({
        //     name: 'GUID',
        //     value: '09031014210007069201&w='+ new Date().getTime(),//'09031014210007069201',//+ parseInt(Math.random()*10),
        //     domain: '.qa.nt.ctripcorp.com',//'.ctrip.com',  测试 .qa.nt.ctripcorp.com
        //     path: '/',
        //     secure: false,
        //     httponly: false,
        //     expires: Date.now() + (1000 * 60 * 60 * 24 * 5)
        // });

        /*
        if (/detail(\.min)?\.css/.test(requestData['url']) || /detail\-base(\.min)?\.css/.test(requestData['url']) || /common\.js/.test(requestData['url']) || /orderdetail-flight\.js/.test(requestData['url'])) {
            request.changeUrl(requestData['url'] + '&tt=' + Date.now());
            console.log('change requested url: ' + requestData.url + ', env: ' + curPage.env + ', orderid: ' + curPage.ord + ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's');
           //console.log('change requested: ' + JSON.stringify(requestData));

        }*/



        var changeurl = '';
        cssData.forEach(function (item) {
            if (item.test(requestData['url'])) {
                //change本地Url，调试成功
                //changeurl = baseUrl + requestData['url'].replace(/[\/\:]/g, '_');
                //console.log('^^^^^changeurl:'+ changeurl);
                //request.changeUrl(changeurl);
                //追加时间
                request.changeUrl(requestData['url'] + '?tt=' + Date.now());
                //console.log('rrrrrrrr change requested url: ' + requestData.url + ', env: ' + curPage.env + ', orderid: ' + orderid + ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's');
                //console.log('change requested: ' + JSON.stringify(requestData));
                consoleData = {
                    changeRequestedUrlCSS: requestData.url,
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    useTime: computeUseTime(Date.now(), curPage.settings.iniTime),
                };
                loggerDebug('request CSS', consoleData);

            }

        });


        jsData.forEach(function (item) {
            if(item.test(requestData['url'])){
                request.changeUrl(requestData['url'] + '?tt=' + Date.now());
                //console.log('change requested url: ' + requestData.url + ', env: ' + curPage.env + ', orderid: ' + curPage.ord + ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's')
                //console.log('change requested: ' + JSON.stringify(requestData));

                consoleData = {
                    changeRequestedUrlJS: requestData.url,
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    useTime: computeUseTime(Date.now(), curPage.settings.iniTime),
                };
                loggerDebug('request JS', consoleData);

            }

        });

        /*
        if (/lizard\.lite\.min\.js/.test(res['url'])) {

            request.changeUrl(requestData['url'] + '?tt=' + Date.now());
            console.log('change requested url: ' + requestData.url + ', env: ' + curPage.env + ', orderid: ' + curPage.ord + ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's')
            //console.log('change requested: ' + JSON.stringify(requestData));
        }*/

        /*
        arrRes.forEach(function(item) {
            if (item.urlRe.test(requestData.url) && (requestData.method === 'POST' || item.method === 'GET')) {
                console.log('requested: ' + requestData.url +
                    ' index: ' + curPage.settings.index +
                    ', env: ' + env +
                    ', orderid: ' + ord +
                    ', number: ' + orderidIndex +
                    ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's');

                //console.log('requested: ' + JSON.stringify(req, undefined, 4)+' index: '+curPage.settings.index);
            }
        });*/
        //console.log('requested: ' + JSON.stringify(req, undefined, 4));


        //arrRes.forEach(function(item){
        //    item.isReqSucc = item.urlRe.test(req.url);
        //    if(item.isReqSucc){
        //        console.log('req');
        //        console.log(req.url);
        //    }
        //
        //});
    };

    curPage.onResourceReceived = function (res) {
        // var hostname = res.url.match(/\/\/.+?\//)[0];
        // if (!/ubt/.test(hostname)) {
        //     console.log('url: ' + res.url+'task: '+page.settings.index);
        // }
        // arrRes.forEach(function(item) {jsData
        //     if (item.urlRe.test(res.url) && !/ubt/.test(hostname) ){
        //         // console.log('url: ' + res.url);
        //         // console.log('contentType: ' + res.contentType + 'index: ' + curPage.settings.index);
        //         // console.log('status: ' + res.status + ' stage: ' + res.stage);
        //         // //console.log('received: ' + JSON.stringify(res, undefined, 4)+ 'index: ' + curPage.settings.index);
        //     }
        // });
        // console.log('*****receive: ' + JSON.stringify(res));
        if (res.url && res.stage !== 'start' && res.contentType != null) {

            pageTask(curPage, arrRes, res, dataInfo, callback);
        }


        /*
        //增加日志记录
        if (/detail(\.min)?\.css/.test(res['url']) || /detail\-base(\.min)?\.css/.test(res['url']) || /common\.js/.test(res['url']) || /orderdetail-flight\.js/.test(res['url'])) {

            console.log('change received url: ' + res.url + ', env: ' + curPage.env + ', orderid: ' + curPage.ord + ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's');
            //console.log('change received: ' + JSON.stringify(res, undefined, 4));
        }

        if (/lizard\.lite\.min\.js/.test(res['url'])) {

            console.log('change received url: ' + res.url + ', env: ' + curPage.env + ', orderid: ' + curPage.ord + ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's');
            //console.log('change received: ' + JSON.stringify(res, undefined, 4));
        }*/
        cssData.forEach(function (item) {
            if (item.test(res['url'])) {
                //console.log('change received url: ' + res.url + ', env: ' + curPage.env + ', orderid: ' + curPage.ord + ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's');
                //console.log('change received: ' + JSON.stringify(res, undefined, 4));
                consoleData = {
                    changeReceivedUrlCSS: res.url,
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    useTime: computeUseTime(Date.now(), curPage.settings.iniTime),
                };
                loggerDebug('received CSS', consoleData);


            }
        });


        jsData.forEach(function (item) {
            if (item.test(res['url'])) {
                //console.log('change received url: ' + res.url + ', env: ' + curPage.env + ', orderid: ' + curPage.ord + ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's');
                //console.log('change received: ' + JSON.stringify(res, undefined, 4));

                consoleData = {
                    changeReceivedUrlJS: res.url,
                    env: dataInfo.env,
                    orderid: dataInfo.orderid,
                    useTime: computeUseTime(Date.now(), curPage.settings.iniTime),
                };
                loggerDebug('received JS', consoleData);

            }

        });

    };
    curPage.open(dataInfo.url, function (status) {

        // console.log('open url ' + dataInfo.url);
        //console.log(status + ': open url, env: ' + env + ', orderid: ' + ord + ', number: ' + orderidIndex);
        consoleData = {
            env: dataInfo.env,
            orderid: dataInfo.orderid,
            number: dataInfo.orderidIndex,
        };
        loggerInfo(status + ': open url ', consoleData);
        if (status == 'success') {

            curPage.evaluate(function () {
                localStorage.clear();
            });
            // var cookies = curPage.cookies;
            //
            // console.log('Listing cookies:');
            // for(var i in cookies) {
            //     console.log(cookies[i].name + '=' + cookies[i].value);
            // }

            //console.log('open url: '+ url + '(env: ' + env +', orderid: ' + ord + ')');
            consoleData = {
                url: dataInfo.url,
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
            };
            loggerDebug('open url', consoleData);

            console.log('start isAllReqSucc');
            //console.log('curPage.settings.userAgent'+curPage.settings.userAgent);
            //console.log('curPage.settings.index'+curPage.settings.index);
            console.log('end isAllReqSucc');
        }
        else {
            console.log('********************fail');
            curPage.render(dataInfo.PAGEPATH +  baseDataInfo.wholePageName + '_timeout.png', {
                format: 'png',
                quality: '50'
            });
            //console.log('save timeout page, useTime: ' + (Date.now() - page.settings.iniTime) / 1000 + 's');
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                useTime: computeUseTime(Date.now(), curPage.settings.iniTime),
            };
            loggerDebug('save timeout page', consoleData);

            //console.log('fail: '+ 'reason: open url fail, env: ' + env + ', orderid: ' + ord + ', number: ' + orderidIndex );
            curPage.close();
            workingTime = (Date.now() - curTime) / 1000;
            //清除超时定时时间
            clearTimeout(curPage.settings.timer);
            consoleData = {
                env: dataInfo.env,
                orderid: dataInfo.orderid,
                number: dataInfo.orderidIndex,
                workingTime: computeUseTime(Date.now(), curTime),
                useTime: computeUseTime(Date.now(), curPage.settings.iniTime),
            };
            callback(null, getCallbackInfo('fail: ', consoleData));
            //callback(null, 'fail: ' + 'env: ' + env + ', orderid: ' + ord + ', number: ' + orderidIndex + ', workingTime: ' + workingTime + 's' + ', useTime: ' + (Date.now() - curPage.settings.iniTime) / 1000 + 's');

        }
    });
}

function startAppTask() {
    console.log('startAppTask');
    var dataInfo = {};
    async.mapLimit(urls, 1, function (url, callback, index) {

        var delay = parseInt((Math.random() * 10000000) % 2000, 10);
        var orderid = orderids.shift();
        var orderidIndex = orderidIndexs.shift();
        var env = envs.shift();
        var pagefile = pagefiles.shift();
        //console.log('open', url, ',time:' + delay + 'ms');

        dataInfo = {
            url: url,
            orderid: orderid,
            env: env,
            pagefile: pagefile,
            orderidIndex: orderidIndex,
            datekey: inputData.datekey,
        };
        handle_page(dataInfo, callback);


    }, function (err, result) {
        console.log('**********************************final result****************************************');
        //console.log('final: ');
        for (var item in result) {
            console.log(result[item]);
        }
        phantom.exit();
    });
}

app();


